export class PatientProcedures {
    performedDateTime:string;
    ProcedureName:string;
    DoctorName:string = 'Doctor Name';
    DoctorProfile:string = 'Doctor Profile';
    id: number;
}

export class PatientDiagnosis {
    diagnosisDate:string;
    DiagnosisDesc:string;
    patientId: string;
    id: number;
}

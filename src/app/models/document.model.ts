
export class Document {
    patientId: string;
    FileName: string;
    Path: string;
    documentType: string;
    proc_Diag_Id: number;
}

import { Component, OnInit, ViewChild, ElementRef, SystemJsNgModuleLoaderConfig } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective } from '@angular/forms';
import {ReasonMasterService} from '../../../services/reason-master.service'
@Component({
  selector: 'app-create-new-reason',
  templateUrl: './create-new-reason.component.html',
  styleUrls: ['./create-new-reason.component.css']
})
export class CreateNewReasonComponent implements OnInit {
  formData = new FormData();
  reasonForm: FormGroup;

  constructor( private router: Router,private formBuilder: FormBuilder,private _reasonmasterService:ReasonMasterService) { }

  ngOnInit() {
    this.reasonForm = this.formBuilder.group({
      REASONID : ['0'],
      REASONCODE : ['', [Validators.required]],
      REASONDESCRIPTOON : ['', [Validators.required]],
      PatientName : [''],
      TYPE : ['', [Validators.required]]
      
  });
  }
  save() {
    
    if (!this.reasonForm.valid) {
        return;
    } else {
      this._reasonmasterService.saveReason(this.reasonForm.value).subscribe((res) => {
        console.log('Response:%o', res);
        this.router.navigateByUrl('/app-reason');
      }, err => {
        console.log(err);
    });
    }
  }
}

import { Component, OnInit } from '@angular/core';
import {BookAppointmentService } from '../../services/book-appointment.service';
import {ReasonMasterService } from '../../services/reason-master.service';
@Component({
  selector: 'app-reason',
  templateUrl: './reason.component.html',
  styleUrls: ['./reason.component.css']
})
export class ReasonComponent implements OnInit {

  constructor(private bookAppointmentService: BookAppointmentService,private _reasonmasterService:ReasonMasterService) { }
  reasons = [];
  ngOnInit() {
  }
  getReason(param: any) {
if(param.target.value=="999"){
  this.reasons = [];
  return;
}
    this.bookAppointmentService.getReason(param.target.value)
      .subscribe((res) => {
        this.reasons = res;

      }, err => {
        console.log(err);
      });
  }
  deletedata:any;
  delete(value:any){
    this.deletedata=value;
    document.getElementById("modalmarkDelete").style.display = "block";
  }
  public closePopuop(myModal: string, roomNo: string) {

    document.getElementById(myModal).style.display = "none";

}

public openmodal(myModal: string, value: any) {
        
        this._reasonmasterService.deleteReason(this.deletedata)
            .subscribe((res) => {
                document.getElementById(myModal).style.display = "none";
                this.reasons = [];


            }, err => {
                console.log(err);
            });
        document.getElementById(myModal).style.display = "block";

    }
}

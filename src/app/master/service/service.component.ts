import { Component, OnInit } from '@angular/core';
import { ServiceMasterService } from 'src/app/services/service-master.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {
  service: [];
  constructor(private serviceMasterService: ServiceMasterService, public toastr: ToastrManager) { }

  ngOnInit() {
    this.getAllService();
  }
  getAllService() {
    this.serviceMasterService.getAllServices()
      .subscribe((res) => {
        this.service = res;
        console.log('Service%o', this.service);
      }, err => {
        console.log(err);
      });
  }
  deleteService(request) {
    this.serviceMasterService.DeleteService(request).subscribe((res) => {
      this.toastr.successToastr("Deleted successfully","success");
      this.getAllService();
    }, err => {
      console.log("error");
    })
  }

}

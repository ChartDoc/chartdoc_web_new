import { Component, OnInit } from '@angular/core';
import { ServiceMasterService } from 'src/app/services/service-master.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-new-service',
  templateUrl: './create-new-service.component.html',
  styleUrls: ['./create-new-service.component.css']
})
export class CreateNewServiceComponent implements OnInit {

  constructor(private serviceMasterService: ServiceMasterService, private router: Router) { }
  serviceName: string;

  ngOnInit() {
  }
  saveService() {
    var data = {ServiceID:"0", ServiceName: this.serviceName }
    this.serviceMasterService.saveService(data).subscribe((res) => {
      this.router.navigate(['/app-service']);
      //alert("save successdully" + res);
    }, err => {
      console.log(err);
    });
  }

}

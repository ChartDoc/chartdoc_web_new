import { Component, OnInit } from '@angular/core';
import { OthersServiceService } from 'src/app/services/others-service.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent implements OnInit {
  Types:any[];
  Others:any[];
  selectedTypeId:string;


  constructor(private OthersService: OthersServiceService,public toastr: ToastrManager) { }

  ngOnInit() {
    this.populatetypes();
  }
  selectChange(event: any) {
    //update the ui
    this.selectedTypeId = event.target.value;
    this.populatetOthers(this.selectedTypeId);
  }
  populatetypes(){
    this.OthersService.getTypes()
    .subscribe((res) => {
      this.Types= res;
    });
  }
  populatetOthers(TypeId:string){
    this.OthersService.getOthers(TypeId)
    .subscribe((res) => {
      this.Others= res;
      //let test = 0;
    }, err => {
      this.toastr.successToastr(err);
    });
  }

  deleteOther(request){
    this.OthersService.deleteOther(request).subscribe((res) => {
      this.toastr.successToastr("Deleted successfully","success");
      this.populatetOthers(this.selectedTypeId);
    }, err => {
      console.log("error");
    })
  }

}
  
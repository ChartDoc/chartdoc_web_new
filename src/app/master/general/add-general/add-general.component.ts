import { Component, OnInit } from '@angular/core';
import {OthersServiceService} from '../../../services/others-service.service'
import { Toastr, ToastrManager } from 'ng6-toastr-notifications';
import { OtherSave } from '../../../models/other-save';
import { TabHeadingDirective } from 'ngx-bootstrap';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-general',
  templateUrl: './add-general.component.html',
  styleUrls: ['./add-general.component.css']
})
export class AddGeneralComponent implements OnInit {

  selectedTypeId = "-1";
  Types:any[];
  OtherValues:OtherSave =new OtherSave();

  constructor(private OthersService: OthersServiceService,public toastr: ToastrManager, private router: Router) { }

  ngOnInit() {
    this.populatetypes();
  }
  selectChangeHandler(event: any) {
    //update the ui
    this.selectedTypeId = event.target.value;
  }

  populatetypes(){
    this.OthersService.getTypes()
    .subscribe((res) => {
      this.Types= res;
    });
  }
  SaveOther(Desc:string){
    let s = 0;
    if(this.selectedTypeId=="" || this.selectedTypeId=="-1"){
      this.toastr.warningToastr("Select Type");
      return;
    }else if(Desc==""){
      this.toastr.warningToastr("Add decsription");
      return;
    }
    else{
      let category: OtherSave = {
        ID: '0',
        Name: Desc,
        TYPE:this.selectedTypeId
    };
      // this.OtherValues.ID="0";
      // this.OtherValues.Name=Desc;
      // this.OtherValues.TYPE=this.selectedTypeId;
      this.OthersService.saveOther(category)
      .subscribe((res)=>{
        this.toastr.successToastr("Added."); 
        this.router.navigate(['/app-general']);
      }, err => {
        this.toastr.successToastr(err);
      });
    }

  }


}

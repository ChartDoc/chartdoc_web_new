import { Component, OnInit } from '@angular/core';
import { PatientInsuranceinfoService } from '../../services/patient-insuranceinfo.service';
import { Insuranceinfo } from '../../models/patient-insuranceinfo.model';
@Component({
  selector: 'app-patient-insurance-info',
  templateUrl: './patient-insurance-info.component.html',
  styleUrls: ['./patient-insurance-info.component.css']
})
export class PatientInsuranceInfoComponent implements OnInit {

  insuranceinfos: any;
  insuranceinfosArray: Insuranceinfo[] = [];

  public insuranceinfosList: any;
  isRecordFound: boolean;
  patientId: string;
  flag: string;
  constructor(private _patientInsuranceinfoService: PatientInsuranceinfoService) { }

  ngOnInit() {
    this.flag = this._patientInsuranceinfoService.getPatientDetails('patientInfo').flag;
    this.patientId = this._patientInsuranceinfoService.getPatientDetails('patientInfo').patientId;
    this._patientInsuranceinfoService.getInsuranceInfo(this.patientId, this.flag)
    .subscribe((res) => {
    
        this.insuranceinfosList = res;
        
      
    },
    err => {
       console.log(err);
    });
  }


}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';
import { PatientdiagnosisService } from '../../services/patientdiagnosis.service';
import { PatientDiagnosis } from 'src/app/models/patient-diagnosis';
import { Document } from 'src/app/models/document.model';
import { isNullOrUndefined } from 'util';


@Component({
  selector: 'app-patient-diagnosis',
  templateUrl: './patient-diagnosis.component.html',
  styleUrls: ['./patient-diagnosis.component.css']
})


export class PatientDiagnosisComponent implements OnInit,OnDestroy {

  DiagnosisData: any;
  DiagnosisEntry: Array<PatientDiagnosis> = [];
  // Diagnosis:PatientDiagnosis = new PatientDiagnosis();
  Diagnosys: string[];
  diagnosisDocument: Array<Document> = [];
  patientInfo: any;
  Commonsubscription: Subscription;
  Apisubscription: Subscription;

  PatientId:string;
  flag: string;
  pipe = new DatePipe('en-US'); // Use your own locale
  IsPosCheckOut:boolean = false;


  constructor(private DiagnosisService:PatientdiagnosisService) { }

  ngOnDestroy(){
    // this.Commonsubscription.unsubscribe();
    // this.Apisubscription.unsubscribe();
  }

  ngOnInit() {
    this.LoadInit();
  }
  LoadInit(){
    let doctorBookingInfo = this.DiagnosisService.getBookingInfo('doctorBookingInfo');
    if(doctorBookingInfo!=null && doctorBookingInfo.positionID!='undefined' && doctorBookingInfo.positionID=='-3'){
      //alert(doctorBookingInfo.positionID);
      this.IsPosCheckOut=true;
    }

    this.patientInfo = this.DiagnosisService.getPatientDetails('patientInfo');
    this.flag = this.patientInfo.flag;
    this.PatientId = this.patientInfo.patientId;
    
  this.Apisubscription = this.DiagnosisService.GetPatientDiagnosis(this.PatientId, this.flag)
      .subscribe((res) => {   
        if(this.flag == 'E'){
          res.entry.forEach(element => {          
          
            this.DiagnosisEntry.push(
              { 
                diagnosisDate: this.pipe.transform(new Date(element.resource.effectiveDateTime), 'MMM dd,yyyy'), 
                DiagnosisDesc: element.resource.code.text,
                patientId: this.PatientId,
                id: 0
              });
          });
        }        
        else{
          res.forEach(item => {
            this.DiagnosisEntry.push(
              {
                diagnosisDate: this.pipe.transform(new Date(item.DiagnosisDate), 'MMM dd,yyyy'), 
                DiagnosisDesc: item.DiagnosisDesc,
                patientId: this.PatientId,
                id:item.Id
              }
            )
          });
        }
        this.DiagnosisService.setDiagnosisDetails('diagnosis' + this.PatientId, this.DiagnosisEntry);
        this.getDocuments(this.DiagnosisEntry[0].id);
      },
      err => {
        console.log(err);
      });
  }

  LoadDiagnosisDocuments(Id:number){
    this.getDocuments(Id);
  }

  getDocuments(id: number){
    this.DiagnosisService.GetDocumentByPatientId(this.PatientId, 'D', id).subscribe((data: Document[]) => {
      if(this.flag == 'E'){
        this.diagnosisDocument.push({
          patientId: this.PatientId,
          proc_Diag_Id: 0,
          FileName: 'Jason Argonaut.pdf',
          Path: '../../../../assets/DiagnosisDocuments/Jason Argonaut.pdf',
          documentType: 'D'
        });
      }
      else{
        this.diagnosisDocument = data;          
      }
    });
    this.DiagnosisService.setDiagnosisDetails('diagnosis-document' + this.PatientId, this.diagnosisDocument);
    
  }

  downloadFile(){
    this.diagnosisDocument.forEach(item => {
      let link = document.createElement("a");
      link.download = item.FileName;
      link.href = item.Path;
      link.click();
      link.remove();
    });
  }

  openModelDiagnosis(myModal: string){
    document.getElementById(myModal).style.display = "block";
    
  }
}

import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { Md5 } from 'ts-md5/dist/md5';
import { CommonService } from 'src/app/core/common.service';
import { SharedService } from 'src/app/core/shared.service';

// const form = new FormGroup({
//   Drugid: new FormControl(),
//   prescriptionDrugName: new FormControl(),
//   prescriptionPrescipbedBy: new FormControl()
// });

@Component({
  selector: 'app-patient-prescriptions',
  templateUrl: './patient-prescriptions.component.html',
  styleUrls: ['./patient-prescriptions.component.css'],
  providers: [ DatePipe ]
})
export class PatientPrescriptionsComponent implements OnInit {

  public parameterString: string;
  mac: string;
  url: string;
  currDate: string;
  patientRcopiaId: string;
  patientRcopiaName: string;
  // prescription: Prescriptions[];
  // prescriptionDrugid: number;
  // PrescriptionsdrugName: string;
  // PrescriptionsprovidedBy: string;
  // PrescriptionsDetails: Array<Prescriptions> = [];
  // public searchTerm: string;
  // public admin = false;
  // public tblvisibility = false;
  // public isvisibility = false;
  // public btndisabled = false;
  // applyAddbuttonAcitiveClass: boolean;
  // clicked: boolean;
  // icdDetailscopy: any;
  // Drugid: number;
  // public id: number;
  // public counter = 0;
  // isIdExist: boolean;

  // tslint:disable-next-line: variable-name
  // private _patientprescription: PatientPrescriptionsservice
  constructor(private sanitizer: DomSanitizer,
    private datePipe: DatePipe, private sharedService: SharedService) { }

  ngOnInit() {
    this.patientRcopiaId = this.sharedService.getLocalItem('patientInfo').rcopiaId;
    this.patientRcopiaName = this.sharedService.getLocalItem('patientInfo').rcopiaName;
  }

  getRcopia(){
    this.currDate = this.datePipe.transform(new Date().toUTCString(), "MMddyyHHmmss", "GMT");
    this.parameterString = "rcopia_portal_system_name=ravendor1259&rcopia_practice_user_name=rc980013&rcopia_user_external_id=provider_rc980013_1571953807&service=rcopia&action=login&startup_screen=patient&rcopia_patient_system_name=" + this.patientRcopiaName +"&rcopia_patient_external_id="+ this.patientRcopiaId +"&skip_auth=n&time=" + this.currDate + "u3k68vgz";
    //this.parameterString = "rcopia_portal_system_name=ravendor1259&rcopia_practice_user_name=rc980013&rcopia_user_external_id=provider_rc980013_1571953807&service=rcopia&action=login&startup_screen=patient&rcopia_patient_system_name=AbramJill&rcopia_patient_external_id=AbramJill03011991&skip_auth=n&time=" + this.currDate + "u3k68vgz";

    this.mac = "&MAC=" + Md5.hashStr(this.parameterString).toString().toUpperCase();
    this.parameterString = this.parameterString.replace('u3k68vgz', this.mac);
    this.url = "https://web4.staging.drfirst.com/sso/portalServices?" + this.parameterString;
    
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
  }
  // Searchclick() {
  //   this.admin = true;
  // }

  // toggole() {
  //   this.admin = true;
  // }

  // AddPrescriptionDetails(ID: number, DrugName: string, PrescriptionProvidedBy: string) {
  //   this.tblvisibility = true;
  //   console.log(ID);
  //   console.log(this.PrescriptionsDetails);
  //   form.patchValue({Drugid: ID, prescriptionDrugName: DrugName, prescriptionPrescipbedBy: PrescriptionProvidedBy});
  //   this.isIdExist = false;
  //   this.PrescriptionsDetails.forEach(element => {
  //     if (element.Drugid === ID) {
  //       console.log(element.Drugid);
  //       this.isIdExist = true;
  //     }
  //   });

  //   if (!this.isIdExist) {
  //     this.PrescriptionsDetails.push(form.value);
  //   }

  //   }

  //   deleteRow(i: number) {
  //     this.PrescriptionsDetails.splice(i, 1);
  //     if (this.PrescriptionsDetails.length < 1) {
  //       this.tblvisibility = false;
  //       this.btndisabled = false;
  //       this.clicked = false;
  //     }
  // }


}

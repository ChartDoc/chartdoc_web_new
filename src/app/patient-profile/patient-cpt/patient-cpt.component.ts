import { Component, OnInit } from '@angular/core';
import { PatientCptService } from '../../services/patient-cpt.service';
import { FormGroup, FormControl } from '@angular/forms';
import { PatientCPTModel } from '../../models/PatientCPT.model';
import { isNullOrUndefined } from 'util';


const form = new FormGroup({
  ID: new FormControl(),
  Code: new FormControl(),
  Desc: new FormControl()
});

@Component({
  selector: 'app-patient-cpt',
  templateUrl: './patient-cpt.component.html',
  styleUrls: ['./patient-cpt.component.css']
})
export class PatientCptComponent implements OnInit {
  cptCode: any;
  cptDesc: any;
  cptDetailss: PatientCPTModel[];
  public admin = false;
  public searchTerm: string;
  public tblvisibility = false;
  isIdExist: boolean;
  formCPT: FormGroup;
  patientInfo: [] = [];
  PatientId: string;
  cptDetails: Array<PatientCPTModel> = [];
  frm: PatientCPTModel[] = [];
  public tblvisibilityOnload = false;
  message: string;
  IsPosCheckOut:boolean = false;

  constructor(private patientCptService: PatientCptService) { }

  ngOnInit() {
    let doctorBookingInfo = this.patientCptService.getBookingInfo('doctorBookingInfo');
    if(doctorBookingInfo!=null && doctorBookingInfo.positionID!='undefined' && doctorBookingInfo.positionID=='-3'){
      //alert(doctorBookingInfo.positionID);
      this.IsPosCheckOut=true;
    }
    
    this.PatientId = this.patientCptService.getPatientDetails('patientInfo').appointmentId;
    if(isNullOrUndefined(this.patientCptService.getAllCpt('AllCpt'))){
      this.patientCptService.getPatientCptDetails()
        .subscribe((res) => {
          this.cptDetailss = res;
          this.patientCptService.setAllCpt('AllCpt', this.cptDetailss);
        },
        err => {
          console.log(err);
        });
    }
    else{
      this.cptDetailss = this.patientCptService.getAllCpt('AllCpt');
    }
    this.tblvisibility = true;


    this.formCPT = new FormGroup({
      ID: new FormControl(),
      patientId: new FormControl(),
      Code: new FormControl(),
      Desc: new FormControl()
    });
 
    if((this.patientCptService.getCptDetailsByPatientId('cpt'+ this.PatientId))){
      this.patientCptService.getSavedCPT(this.PatientId)
        .subscribe((data) => {
          this.cptDetails = data;
          this.patientCptService.setCptDetailsByPatientId('cpt'+ this.PatientId, this.cptDetails);
        },
        err => {
          console.log(err);
        });
    }
    else{
      this.patientCptService.getCptDetailsByPatientId('cpt'+ this.PatientId);
    }
  }

  AddCPTDetails(cptId: string, cptCode: string, cptDesc: string) {
    this.tblvisibility = true;
    form.patchValue({ID: cptId, Code: cptCode, Desc: cptDesc});

    this.isIdExist = false;
    this.cptDetails.forEach(element => {
      if (element.ID === cptId) {
        this.isIdExist = true;
      }
    });

    if (!this.isIdExist) {
      this.cptDetails.push(form.value);
      this.patientCptService.setCptDetailsByPatientId('cpt'+ this.PatientId, this.cptDetails);
    }
  }

  deleteRow(cpt: PatientCPTModel) {
    const index: number = this.cptDetails.indexOf(cpt);
    this.cptDetails.splice(index, 1);
    if (this.cptDetails.length < 1) {
      this.tblvisibility = false;
    }
  }

  public saveCptDetails(e: Event) {
    e.preventDefault();
    this.cptDetails.forEach(item => {
      this.formCPT.patchValue({
        Code: item.Code,
        Desc: item.Desc,
        patientId: this.PatientId,
        ID: item.ID
      });
      this.frm.push(this.formCPT.value);
    });
    
    this.patientCptService.saveCptDetails(this.frm)
    .subscribe(
      data => {
        if(data > 0){
          this.patientCptService.setCptDetailsByPatientId('cpt'+ this.PatientId, this.cptDetails);
          this.message = "Operation Successful";
        }
        else{
          this.message = "Operation Unsuccessful";
        }
      }
    );
  }
}


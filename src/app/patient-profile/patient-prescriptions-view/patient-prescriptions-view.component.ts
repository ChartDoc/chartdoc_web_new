import { Component, OnInit } from '@angular/core';
import {PatientPrescriptionsservice  } from '../../services/patient-Prescriptions.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-patient-prescriptions-view',
  templateUrl: './patient-prescriptions-view.component.html',
  styleUrls: ['./patient-prescriptions-view.component.css']
})
export class PatientPrescriptionsViewComponent implements OnInit {
  patientInfo: any;
  PatientId:string;
  Apisubscription: Subscription;
  prescriptionData:any
  constructor(private _prescriptionsservice:PatientPrescriptionsservice) { }

  ngOnInit() {
    this.patientInfo = this._prescriptionsservice.getPatientDetails('patientInfo');
    this.PatientId = this.patientInfo.patientId;
    this._prescriptionsservice.GetPatientGetMedicine(this.PatientId)
      .subscribe((res) => {  
        this.prescriptionData=res;
      },
      err => {
        console.log(err);
      });
    
  }

}

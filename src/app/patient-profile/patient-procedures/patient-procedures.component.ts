import { Component, OnInit, OnDestroy   } from '@angular/core';
import {PatientProceduresService} from '../../services/patient-procedures.service'
import { Subscription } from 'rxjs';
import { PatientProcedures } from 'src/app/models/patient-procedures';
import { DatePipe } from '@angular/common';
import { isNullOrUndefined } from 'util';
import { Document } from 'src/app/models/document.model';

@Component({
  selector: 'app-patient-procedures',
  templateUrl: './patient-procedures.component.html',
  styleUrls: ['./patient-procedures.component.css']
})
export class PatientProceduresComponent implements OnInit,OnDestroy {
  data: any;
  procedureDocument: Array<Document> = [];
  isViewByDate:Boolean = true;
  Commonsubscription: Subscription;
  Apisubscription: Subscription;
  ProceduresEntry: Array<PatientProcedures> = [];
  PatientId:string;
  pipe = new DatePipe('en-US'); // Use your own locale
  value:number = 0;
  flag: string;
  IsPosCheckOut:boolean = false;


  constructor(private ProcedureService:PatientProceduresService) { }

  ngOnDestroy(){
    //this.Commonsubscription.unsubscribe();
    //this.Apisubscription.unsubscribe();
  }

  ngOnInit() {
    this.LoadInit();  
  }
  LoadInit(){
    let doctorBookingInfo = this.ProcedureService.getBookingInfo('doctorBookingInfo');
    if(doctorBookingInfo!=null && doctorBookingInfo.positionID!='undefined' && doctorBookingInfo.positionID=='-3'){
      //alert(doctorBookingInfo.positionID);
      this.IsPosCheckOut=true;
    }
    
    this.PatientId = this.ProcedureService.getPatientDetails('patientInfo').patientId;
    this.flag = this.ProcedureService.getPatientDetails('patientInfo').flag;

    this.Apisubscription = this.ProcedureService.GetPatientProcedures(this.PatientId, this.flag)
      .subscribe((res) => {
        if(this.flag == 'E'){
          res.entry.forEach(element => {
            this.ProceduresEntry.push(
              { 
                performedDateTime: this.pipe.transform(new Date(element.resource.performedDateTime), 'MMM dd,yyyy'), 
                ProcedureName: element.resource.code.text,
                DoctorName:this.value==0?'Dr.June Abigail':(this.value == 1? 'Dr. Benjamin Dartmoth':(this.value == 2?'Dr. Sanjay Gupta':(this.value==3?'Dr. Susan Mendenhall':'Dr. Michael Svenson'))),
                DoctorProfile:this.value==0?'Oncologist':(this.value==1?'Cardiologist':(this.value==2?'Gastroentologist':(this.value==3?'Primary Care Physician':'Cardiologist'))),
                id: 0
            });
            this.value++;
          });
        }else{
          res.forEach(item => {
            this.ProceduresEntry.push(
              { 
                performedDateTime: this.pipe.transform(new Date(item.ProcedureDate), 'MMM dd,yyyy'), 
                ProcedureName: item.ProcedureDesc,
                DoctorName:item.Dr_Name,
                DoctorProfile:item.Dr_Profile,
                id:item.Id
            });
            this.value++;
          });
        }
        this.ProcedureService.setProcedureDetails('procedures' + this.PatientId, this.ProceduresEntry);
        this.getDocuments(this.ProceduresEntry[0].id);
      },
      err => {
        console.log(err);
      });
  }


  getDocuments(id: number){
    this.ProcedureService.GetDocumentByPatientId(this.PatientId, 'P', id).subscribe((data: Document[]) => {
      if(this.flag == 'E'){
        this.procedureDocument.push({
          patientId: this.PatientId,
          proc_Diag_Id: 0,
          FileName: 'Jason Argonaut.pdf',
          Path: '../../../../assets/ProcedureDocuments/Jason Argonaut.pdf',
          documentType: 'P'
        });
      }
      else{
        this.procedureDocument = data;          
      }
      this.ProcedureService.setDocumentDetails('procedure-document' + this.PatientId, this.procedureDocument);
    });   
  }

  ChangeProcedureView(value:string){
    if(value=='DATE')
    this.isViewByDate = true;
    else
    this.isViewByDate=false;
  }
  LoadProcedureDocuments(id:number){
    this.getDocuments(id);
  }
  openModelProcedure(myModal: string){
    document.getElementById(myModal).style.display = "block";
    
    
  }

  downloadFile(){
    this.procedureDocument.forEach(item => {
      let link = document.createElement("a");
      link.download = item.FileName;
      link.href = item.Path;
      link.click();
      link.remove();
    });
  }
}

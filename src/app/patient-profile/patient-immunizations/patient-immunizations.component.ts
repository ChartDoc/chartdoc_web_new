import { Component, OnInit } from '@angular/core';
import { PatientImmunizationService } from '../../services/patient-immunizations.service';
import { Immunization } from '../../models/patient-immunizations.model';

@Component({
  selector: 'app-patient-immunizations',
  templateUrl: './patient-immunizations.component.html',
  styleUrls: ['./patient-immunizations.component.css']
})
export class PatientImmunizationsComponent implements OnInit {

  immunizations: any;
  immunizationsArray: Immunization[] = [];

  public immunizationsList: any;
  isRecordFound: boolean;
  patientId: string;
  flag: string;

  constructor(private _Immunizationservice: PatientImmunizationService) { }

  ngOnInit() {
    this.flag = this._Immunizationservice.getPatientDetails('patientInfo').flag;
    this.patientId = this._Immunizationservice.getPatientDetails('patientInfo').patientId;
    this._Immunizationservice.getImmunizations(this.patientId, this.flag)
    .subscribe((res) => {
      if (this.flag === 'E') {
        this.immunizationsList = res.entry;
        if (this.immunizationsList.length > 0) {
          this.isRecordFound = true;
          this.immunizationsList.forEach(immunization => {
          this.immunizationsArray.push({ImmunizationsDate: immunization.resource.date, ImmunizationsDesc: immunization.resource.vaccineCode.text});
          });
        }
      } else {
        this.immunizationsList = res;
        this.immunizationsList.forEach(element => {
        this.immunizationsArray.push({ImmunizationsDate: element.Date, ImmunizationsDesc: element.Description});
        });
      }
    },
    err => {
       console.log(err);
    });
  }


}

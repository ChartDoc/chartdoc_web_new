import { Component, OnInit, OnDestroy } from '@angular/core';
import {PatientEncounterService} from '../../services/patient-encounter.service';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { encodeBase64 } from '@progress/kendo-file-saver';
import { Encountermodel } from 'src/app/models/encountermodel';


@Component({
  selector: 'app-patient-encounter',
  templateUrl: './patient-encounter.component.html',
  styleUrls: ['./patient-encounter.component.css']
})
export class PatientEncounterComponent implements OnInit ,OnDestroy {
  Encounters: string[];
  formEncounter:FormGroup;
  public searchString: string;
  encounNotes:string ="";
  patientInfo: any;
  doctorId: number;

  PatientId:string;  
  AppointmentId:string;
  Commonsubscription: Subscription;
  Apisubscription: Subscription;

  encModel:Array<Encountermodel> = [];
  pipe = new DatePipe('en-US'); // Use your own locale
  IsPosCheckOut:boolean = false;

  EncounterSummary:string = '';

  ngOnInit(){

   }

   ngOnDestroy(){
    //this.Commonsubscription.unsubscribe();
    //this.Apisubscription.unsubscribe();
  }

  constructor(private encounterService:PatientEncounterService) {
    this.encModel = []; 

    this.patientInfo = this.encounterService.getPatientDetails('patientInfo');
    this.PatientId = this.patientInfo.patientId;
    this.AppointmentId=this.patientInfo.appointmentId
   // alert("appointmentId="+this.AppointmentId)
    this.doctorId = this.encounterService.getDoctorDetails('doctorInfo').doctorId;

    let doctorBookingInfo = this.encounterService.getBookingInfo('doctorBookingInfo');
    if(doctorBookingInfo!=null && doctorBookingInfo.positionID!='undefined' && doctorBookingInfo.positionID=='-3'){
      //alert(doctorBookingInfo.positionID);
      this.IsPosCheckOut=true;
    }
    
    this.formEncounter = new FormGroup({
      EncounterNote: new FormControl("")  ,
      Summary:new FormControl(""),
      PatientId:new FormControl(""),
      doctorId: new FormControl("")  
    });    

    this.loadEncounters();

    this.formEncounter.get("EncounterNote").setValue(this.encounterService.getEncounterNote('encounterNote' + this.doctorId + this.AppointmentId));
  }

  UsePreviousNote(Notes: string,summary:string){
    this.EncounterSummary = summary;
    this.formEncounter.get("EncounterNote").setValue(summary+"\n\n"+Notes);
    this.encounterService.setEncounterNote('encounterNote' + this.doctorId + this.AppointmentId, Notes)
    this.encounNotes = summary+"\n\n"+Notes;
  }

  submitEncounterSummary(EncounterSummary: string,myModal: string){    
    document.getElementById(myModal).style.display = "none";   

      this.formEncounter.setValue({
        EncounterNote: this.encounterService.getEncounterNote('encounterNote' + this.doctorId +this.AppointmentId),
        Summary:EncounterSummary,
        PatientId:this.AppointmentId,
        doctorId: this.doctorId
      } 
    )
    
    this.encounterService.saveEncounter(this.formEncounter.value).subscribe
    (
      data=>
      {
        this.encounterService.removeLocalEncounter('encounterNote' + this.doctorId + this.AppointmentId);
        this.encModel = []; 
        this.loadEncounters();  
        this.formEncounter.get("EncounterNote").setValue("");
      }      
    ) 

    this.EncounterSummary = '';    
    
  
    
  }

  UpdateEncounterNotes(value:string){
    this.encounterService.setEncounterNote('encounterNote' + this.doctorId + this.AppointmentId, value);
  }

  public OpenEncounterSummary(myModal: string) {
    // this.EncounterSummary
    document.getElementById(myModal).style.display = "block";
  }

  public closeEncounterSummary(myModal: string) {
    document.getElementById(myModal).style.display = "none";
  }

loadEncounters(){  
    this.Apisubscription = this.encounterService.GetPatientEncounters(this.PatientId)
    .subscribe((res) => {
      res.forEach(element => {
      this.encModel.push(
      {
        EncounterDate: this.pipe.transform(new Date(element.EncounterDate), 'MMM dd,yyyy'),
        Summary:element.Summary,
        Name:element.Name,
        EncounterNote:element.EncounterNote,
        ID:element.ID
      });
    });
            
  },
  err => {
    console.log(err);
  });
}

}

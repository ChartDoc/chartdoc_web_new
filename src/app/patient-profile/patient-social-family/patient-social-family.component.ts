import { Component, OnInit } from '@angular/core';
import { PatientSocialFamilyService } from '../../services/patient-social-family.service';
import { PatientSocialModel } from '../../models/patient-social.model';
import { PatientFamilyModel } from '../../models/patient-family.model';
import { CommonService } from 'src/app/core/common.service';

@Component({
  selector: 'app-patient-social-family',
  templateUrl: './patient-social-family.component.html',
  styleUrls: ['./patient-social-family.component.css']
})
export class PatientSocialFamilyComponent implements OnInit {

  patientsocialss: PatientSocialModel[] = [];

  patientFamilyss: PatientFamilyModel[] = [];
  flag: any;
  patientId: any;

  patientsocials: PatientSocialModel[] = [
    {
      Addiection: 'Tobacco',
      Frequency: '1 pack/day',
      Duration: '8 years'
    },
    {
      Addiection: 'Alcohol',
      Frequency: '6 per week',
      Duration: '4 years'
    },
    {
      Addiection: 'Alcohol Test',
      Frequency: '6 per week Test',
      Duration: '4 years Test'
  }
  ];
  patientFamilys: PatientFamilyModel[] = [
    {
      Member: 'Father',
      Diseases: 'Prostate Cancer'
    },
    {
        Member: 'Father',
        Diseases: 'Hypertension'
    },
    {
      Member: 'Father Mother Test',
      Diseases: 'Hypertension Test'
  }
  ];

  // tslint:disable-next-line: variable-name
  constructor(private _patientSocialFamilyService: PatientSocialFamilyService, private commonService: CommonService) { }

  ngOnInit() {
    this.flag = this._patientSocialFamilyService.getPatientDetails('patientInfo').flag;
    this.patientId = this._patientSocialFamilyService.getPatientDetails('patientInfo').patientId;
    this._patientSocialFamilyService.getSocialDetails(this.patientId, this.flag)
    .subscribe((res) => {
      // if (this.flag === 'L') {
      //   this.patientsocialss = this.patientsocials;
      //   } else {
          this.patientsocialss = res;
          console.log(this.patientsocialss);
       //}
    },
    err => {
      console.log(err);
    });

    this._patientSocialFamilyService.getFamilyDetails(this.patientId, this.flag)
    .subscribe((res) => {
      if (this.flag === 'E') {
      this.patientFamilyss = this.patientFamilys;
      } else {
        this.patientFamilyss = res;
        console.log(this.patientFamilyss);
      }
    },
    err => {
      console.log(err);
    });


  }

}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl } from '@angular/forms';
import { PatientProceduresService } from 'src/app/services/patient-procedures.service';
import {  patientProcedureupload} from '../../models/patient-Procedure-upload';

@Component({
  selector: 'app-patient-procedure-upload',
  templateUrl: './patient-procedure-upload.component.html',
  styleUrls: ['./patient-procedure-upload.component.css']
})
export class PatientProcedureUploadComponent implements OnInit {

  DoctorName:string;
  ProcedureDate:any;
  ProcedureDesc:string;
  files: any[] = [];
  patientId:string;
  ProcFormGroup: FormGroup; 
  formData:FormData;
  pipe = new DatePipe('en-US');
  AppointmentId:string;
  
  @Output('LoadInit') LoadInit: EventEmitter<any> = new EventEmitter();

  constructor(private ProcedureService: PatientProceduresService,public toastr: ToastrManager) { }

  ngOnInit() {
    let doctorBookingInfo = this.ProcedureService.getBookingInfo('doctorBookingInfo');
    this.patientId = doctorBookingInfo.patientId;    
    this.AppointmentId = doctorBookingInfo.appointmentid;
    this.DoctorName = doctorBookingInfo.doctorname.substring(0, doctorBookingInfo.doctorname.indexOf('(')-1);

    this.ProcFormGroup = new FormGroup({
      DoctorName:new FormControl(''),
      ProcedureDate:new FormControl(''),
      ProcedureDesc:new FormControl('')
    });
  }

  closeModelProcedure(myModal: string) {
    document.getElementById(myModal).style.display = "none";
  }

  SaveProcedureUpload(myModal: string){
    this.formData = new FormData();

    var param: patientProcedureupload = {
      Dr_Name:this.DoctorName,
      ProcedureDate: this.pipe.transform(new Date(this.ProcFormGroup.controls['ProcedureDate'].value), 'MMM dd,yyyy'),
      ProcedureDesc:this.ProcedureDesc,
      patientId: this.patientId,
      id: 0
    };

    for(let i =0; i < this.files.length; i++){
      this.formData.append(this.files[i].name,this.files[i]);
    }
 
    this.formData.append("ProcUploadDetails",JSON.stringify(param));
    this.formData.append("AppointmentId",JSON.stringify(this.AppointmentId));
    
    this.ProcedureService.saveProcedureDetails(this.formData).subscribe(res => {
      let restatus = res.split('|');
      if (restatus[0] == "1") {
        this.toastr.successToastr("Document uploaded!", 'Success!');
      } else {
        this.toastr.errorToastr(restatus[1] + " , please contact system admin!", 'Error!');
      }
      this.LoadInit.emit();
      document.getElementById(myModal).style.display = "none";
      // window.location.reload();
    });
      // this.ProcedureService.setProcedureDetails('LoadViewHistory','ViewProcedure');
      
    };

}

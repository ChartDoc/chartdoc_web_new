
export class PatientDetail {
    PatientId: string;
    FirstName: string;
    MiddleName: string;
    LastName: string;
    AddressLine: string;
    AddressLine1: string;
    AddressCity: string;
    AddressState: string;
    AddressPostalCode: string;
    AddressCountry: string;
    DOB: string;
    Gender: string;
    Email: string;
    MobNo: string;
    PrimaryPhone: string;
    SecondaryPhone: string;
    ImageName: string;
    ImagePath: string;
    Flag: string;
    Age: string;
    RecopiaID: string;
    RecopiaName: string;
}

export class EmployerContact {
    PatientId: string;
    Name: string;
    Phone: string;
    Address: string;
}

export class EmergencyContact{
    PatientId: string;
    ContactName: string;
    ContactPhone: string;
    Relationship: string;
}

export class Social{
    PatientId: string;
    Marital_Status: string;
    Guardian_FName: string;
    Guardian_LName: string;
    Add_Line: string;
    Add_City: string;
    Add_State: string;
    Add_Zip: string;
    DOB: string;
    Patient_SSN: string;
    Phone_Number: string;
    Guardian_SSN: string;
    Drivers_License_FilePath: string;
    Race: string;
    Ethicity: string;
    Language: string;
    Comm_Mode: string;
}

export class Billing{
    PatientId: string; 
    Billing_Party: string;
    First_Name: string;
    Middle_Name: string;
    Last_Name: string;
    DOB: string;
    Add_Line: string;
    Add_Line1: string;
    Add_City: string;
    Add_State: string;
    Add_Zip: string;
    SSN: string;
    Drivers_License_FilePath: string;
    Primary_Phone: string;
    Secondary_Phone: string;
}

export class Insurance{
    PatientId: string;
    Provider_Id: string;
    Provider_Name: string;
    Insurance_Policy: string;
    Policy_Type: string;
    Policy_Type_Id: string;
    Card_Image_FilePath: string;
    Effective_From: string;
    Status: string;
    Status_Id: string;
    Files: File;
}

export class Authorization{
    PatientId:string;
    Authorization_FilePath:string;
}
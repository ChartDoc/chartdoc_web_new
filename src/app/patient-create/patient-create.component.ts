import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PatientDetail, EmployerContact, EmergencyContact, Social, Billing, Insurance, Authorization } from './patient-model';

import { SharedService } from 'src/app/core/shared.service';
import { PatientCreateService } from '../services/patient-create.service'
import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-patient-create',
  templateUrl: './patient-create.component.html',
  styleUrls: ['./patient-create.component.css']
})
export class PatientCreateComponent implements OnInit {
  formData: FormData;
  socialfiles: File[] = [];
  billingfiles: File[] = [];
  insurancefiles: File[] = [];
  authorizationfiles: File[] = [];
  public socialwidth: number = 200;
  public billingwidth: number = 150;
  billingfileUrl: string = "";
  billingisEdit: boolean = false;
  socialfileUrl: string = "";
  socialIsEdit: boolean = false;
  authorizationfileUrl: string = "";
  authozationIsEdit: boolean = false;
  insuranceArray: Array<Insurance> = [];
  relationshipWithPatient = [];
  state = [];
  race = [];
  ethicity = [];
  preferredLanguage = [];
  preferredModeOfCommunication = [];
  providerName = [];
  provider_Name = '';
  policy_Type = '';
  status = '';
  minorStatus: Boolean = false;
  authorizationSelectedFile: File = null;
  patientId = '0';
  ClsCreateUpdatePatient = {
    sPatientDetails: {},
    sPatientEmpContact: {},
    sPatientEmergency: {},
    sPatientSocial: {},
    sPatientBilling: {},
    sPatientInsurance: [],
    sPatientAuthorisation: {}
  }

  patient_FormGroup = new FormGroup({
    firstName: new FormControl(''),
    middleName: new FormControl(''),
    lastName: new FormControl(''),
    gender: new FormControl(''),
    DOB: new FormControl('')
  });

  patientContact_FormGroup = new FormGroup({
    contact_MobNo: new FormControl(''),
    contact_Phone: new FormControl(''),
    contact_Email: new FormControl(''),
    contact_AddressLine: new FormControl(''),
    contact_AddressLine1: new FormControl(''),
    contact_AddressCity: new FormControl(''),
    contact_AddressState: new FormControl(''),
    contact_AddressPostalCode: new FormControl(''),
    contact_PrimaryPhone: new FormControl(''),
    contact_SecondaryPhone: new FormControl(''),
    contact_Active: new FormControl(''),
  });

  patientEmergency_FormGroup = new FormGroup({
    emergency_ContactName: new FormControl(''),
    emergency_ContactPhone: new FormControl(''),
    emergency_Relationship: new FormControl(''),
    employer_FullName: new FormControl(''),
    employer_Phone: new FormControl(''),
    employer_Address: new FormControl('')
  });

  patientSocial1_FormGroup = new FormGroup({
    social_MaritalStatus: new FormControl(''),
    social_MaritalStatusOther: new FormControl('')
  });

  patientSocial2_FormGroup = new FormGroup({
    social_GuardianFirstName: new FormControl(''),
    social_GuardianLastName: new FormControl(''),
    social_AddressLine: new FormControl(''),
    social_AddressCity: new FormControl(''),
    social_AddressState: new FormControl(''),
    social_AddressZip: new FormControl('')
  });

  patientSocial3_FormGroup = new FormGroup({
    social_DOB: new FormControl(''),
    social_GuardianSSN: new FormControl(''),
    social_PhoneNumber: new FormControl('')
  });

  patientSocial4_FormGroup = new FormGroup({
    social_PatientSSN: new FormControl(''),
    social_DriversLicenseFilePath: new FormControl(''),
    social_Race: new FormControl(''),
    social_Language: new FormControl(''),
    social_Ethicity: new FormControl(''),
    social_CommMode: new FormControl('')
  });

  patientBilling_FormGroup = new FormGroup({
    billingParty: new FormControl(''),
    billingPartyOther: new FormControl(''),
    billing_FirstName: new FormControl(''),
    billing_MiddleName: new FormControl(''),
    billing_LastName: new FormControl(''),
    billing_DOB: new FormControl(''),
    billing_AddressLine: new FormControl(''),
    billing_AddressLine1: new FormControl(''),
    billing_AddressCity: new FormControl(''),
    billing_AddressState: new FormControl(''),
    billing_AddressZip: new FormControl(''),
    billing_SSN: new FormControl(''),
    billing_DriversLicenseFilePath: new FormControl(''),
    billing_PrimaryPhone: new FormControl(''),
    billing_SecondaryPhone: new FormControl('')
  });

  patientInsurance_FormGroup = new FormGroup({
    insurance_ProviderName: new FormControl(''),
    insurancePolicy: new FormControl(''),
    insurance_PolicyType: new FormControl(''),
    insurance_CardImageFilePath: new FormControl(''),
    insurance_EffectiveFrom: new FormControl(''),
    insurance_Status: new FormControl('')
  });

  patientAuthorization_FormGroup = new FormGroup({
    authorizationFilePath: new FormControl('')
  });
  patientimage: any = "noimage.png";
  constructor(private patientCreateService: PatientCreateService,
    private _avRoute: ActivatedRoute, private router: Router, private sharedService: SharedService
    , public toastr: ToastrManager) {
    this.formData = new FormData();
  }

  ngOnInit() {
    if (this._avRoute.snapshot.queryParams["pid"]) {
      this.patientId = this._avRoute.snapshot.queryParams["pid"];
      this.getPatientInfoByPatientId(this.patientId);
    }
    this.getMasterData('7');
    this.getMasterData('1');
    this.getMasterData('2');
    this.getMasterData('3');
    this.getMasterData('4');
    this.getMasterData('5');
    this.getMasterData('6');
    this.getMasterData('7');
  }

  onAuthorizationFileSelected(event) {
    this.authorizationSelectedFile = event.target.files[0];
    const formData_Authorization = new FormData();
    formData_Authorization.append(this.authorizationSelectedFile.name, this.authorizationSelectedFile, this.authorizationSelectedFile.name);
    this.patientCreateService.fileUpload(formData_Authorization).subscribe(
      res => {
        console.log(res)
      },
      err => {
        this.toastr.errorToastr("please contact system admin!", 'Error!');
        console.log(err);
      }
    );
    console.log('this.insuranceSelectedFile:%o', this.authorizationSelectedFile);
  }

  addPolicy() {
    var policy_type_id = this.patientInsurance_FormGroup.value["insurance_PolicyType"];
    let data = undefined;
    if (policy_type_id === '1') {
      data = this.insuranceArray.find(ob => ob['Policy_Type_Id'] === policy_type_id);
    }
    if (data === undefined) {

      var insurance: Insurance = {
        PatientId: this.patientId,
        Provider_Id: this.patientInsurance_FormGroup.value["insurance_ProviderName"],
        Provider_Name: this.provider_Name,
        Insurance_Policy: this.patientInsurance_FormGroup.value["insurancePolicy"],
        Policy_Type_Id: this.patientInsurance_FormGroup.value["insurance_PolicyType"],
        Policy_Type: this.policy_Type,
        Card_Image_FilePath: this.insurancefiles[0].name,//this.patientInsurance_FormGroup.value["insurance_CardImageFilePath"],
        Effective_From: this.patientInsurance_FormGroup.value["insurance_EffectiveFrom"],
        Status_Id: this.patientInsurance_FormGroup.value["insurance_Status"],
        Status: this.status,
        Files: this.insurancefiles[0]
      };
      this.insuranceArray.push(insurance);
    } else {
      this.toastr.errorToastr("Primary insurance policy already exist!", 'Error!');

    }
    this.insurancefiles = [];
  }
  providerChangeHandler(event) {
    this.provider_Name = event.target.options[event.target.options.selectedIndex].text;
    // this.provider_Id=parseInt(event.target.value);
  }
  policyTypeChangeHandler(event) {
    this.policy_Type = event.target.options[event.target.options.selectedIndex].text;
  }
  statusChangeHandler(event) {
    this.status = event.target.options[event.target.options.selectedIndex].text;
  }
  checkMinor() {
    //alert("checkminor"+this.minorStatus)
  }


  finish() {

    this.ClsCreateUpdatePatient.sPatientDetails = this.getPatient();
    this.ClsCreateUpdatePatient.sPatientEmpContact = this.getEmployerContact();
    this.ClsCreateUpdatePatient.sPatientEmergency = this.getPatientEmergency();
    this.ClsCreateUpdatePatient.sPatientSocial = this.getPatientSocial();
    this.ClsCreateUpdatePatient.sPatientBilling = this.getPatientBilling();
    this.ClsCreateUpdatePatient.sPatientInsurance = this.getPatientInsurance();
    this.ClsCreateUpdatePatient.sPatientAuthorisation = this.getPatientAuthorization();
    if (this.ClsCreateUpdatePatient.sPatientInsurance.length == 0) {
      this.toastr.errorToastr("Please Add Insurance!", 'Error!');
      return
    }
    this.formData.append("createUpdatePatient", JSON.stringify(this.ClsCreateUpdatePatient));
    this.formData.append("social", this.socialfiles[0]);
    this.formData.append("billing", this.billingfiles[0]);
    let index = 0
    this.ClsCreateUpdatePatient.sPatientInsurance.forEach(element => {
      index++
      this.formData.append("insurances_" + String(index), element.Files);
    });
    // this.insurancefiles.forEach(element => {
    //   index++
    //   this.formData.append("insurances_"+String(index),element);
    // });
    this.formData.append("authorization", this.authorizationfiles[0]);
    console.log(this.formData.get('createUpdatePatient'));


    this.patientCreateService.savePatient(this.formData)
      .subscribe
      (
        res => {
          //console.log('Response:%o', res);
          var patientTemp = {
            PatientId: res,
            FirstName: this.patient_FormGroup.value["firstName"],
            MiddleName: this.patient_FormGroup.value["middleName"],
            LastName: this.patient_FormGroup.value["lastName"],
            DOB: this.patient_FormGroup.value["DOB"],
            Gender: this.patient_FormGroup.value["gender"],
            ImagePath: this.patientimage
          };
          this.sharedService.setLocalItem("patientDetail", patientTemp);
          this.router.navigate(['/patient-others']);
        },
        err => {
          this.toastr.errorToastr("please contact system admin!", 'Error!');
          console.log(err);
        }
      );
  }

  getPatient() {
    var patient: PatientDetail = {
      PatientId: this.patientId,
      FirstName: this.patient_FormGroup.value["firstName"],
      MiddleName: this.patient_FormGroup.value["middleName"],
      LastName: this.patient_FormGroup.value["lastName"],
      AddressLine: this.patientContact_FormGroup.value["contact_AddressLine"],
      AddressLine1: this.patientContact_FormGroup.value["contact_AddressLine1"],
      AddressCity: this.patientContact_FormGroup.value["contact_AddressCity"],
      AddressState: this.patientContact_FormGroup.value["contact_AddressState"],
      AddressPostalCode: this.patientContact_FormGroup.value["contact_AddressPostalCode"],
      AddressCountry: '',
      DOB: this.patient_FormGroup.value["DOB"],
      Gender: this.patient_FormGroup.value["gender"],
      Email: this.patientContact_FormGroup.value["contact_Email"],
      MobNo: this.patientContact_FormGroup.value["contact_PrimaryPhone"],
      PrimaryPhone: this.patientContact_FormGroup.value["contact_PrimaryPhone"],
      SecondaryPhone: this.patientContact_FormGroup.value["contact_SecondaryPhone"],
      ImageName: '',
      ImagePath: '',
      Flag: this.patientContact_FormGroup.value["contact_Active"],
      Age: '',
      RecopiaID: '',
      RecopiaName: ''
    };
    return patient;
  }

  getPatientEmergency() {
    var emergencyContact: EmergencyContact = {
      PatientId: this.patientId,
      ContactName: this.patientEmergency_FormGroup.value["emergency_ContactName"],
      ContactPhone: this.patientEmergency_FormGroup.value["emergency_ContactPhone"],
      Relationship: this.patientEmergency_FormGroup.value["emergency_Relationship"]
    };
    return emergencyContact;
  }

  getEmployerContact() {
    var employerContact: EmployerContact = {
      PatientId: this.patientId,
      Name: this.patientEmergency_FormGroup.value["employer_FullName"],
      Phone: this.patientEmergency_FormGroup.value["employer_Phone"],
      Address: this.patientEmergency_FormGroup.value["employer_Address"]
    };
    return employerContact;
  };

  getPatientSocial() {
    var social: Social = {
      PatientId: this.patientId,
      Marital_Status: this.patientSocial1_FormGroup.value["social_MaritalStatus"],
      Guardian_FName: this.patientSocial2_FormGroup.value["social_GuardianFirstName"],
      Guardian_LName: this.patientSocial2_FormGroup.value["social_GuardianLastName"],
      Add_Line: this.patientSocial2_FormGroup.value["social_AddressLine"],
      Add_City: this.patientSocial2_FormGroup.value["social_AddressCity"],
      Add_State: this.patientSocial2_FormGroup.value["social_AddressState"],
      Add_Zip: this.patientSocial2_FormGroup.value["social_AddressZip"],
      DOB: this.patientSocial3_FormGroup.value["social_DOB"],
      Patient_SSN: this.patientSocial4_FormGroup.value["social_PatientSSN"],
      Phone_Number: this.patientSocial3_FormGroup.value["social_PhoneNumber"],
      Guardian_SSN: this.patientSocial3_FormGroup.value["social_GuardianSSN"],
      Drivers_License_FilePath: this.patientSocial4_FormGroup.value["social_DriversLicenseFilePath"],
      Race: this.patientSocial4_FormGroup.value["social_Race"],
      Ethicity: this.patientSocial4_FormGroup.value["social_Ethicity"],
      Language: this.patientSocial4_FormGroup.value["social_Language"],
      Comm_Mode: this.patientSocial4_FormGroup.value["social_CommMode"],
    };
    return social;
  }
  getPatientBilling() {
    var billing: Billing = {
      PatientId: this.patientId,
      Billing_Party: this.patientBilling_FormGroup.value["billingParty"],
      First_Name: this.patientBilling_FormGroup.value["billing_FirstName"],
      Middle_Name: this.patientBilling_FormGroup.value["billing_MiddleName"],
      Last_Name: this.patientBilling_FormGroup.value["billing_LastName"],
      DOB: this.patientBilling_FormGroup.value["billing_DOB"],
      Add_Line: this.patientBilling_FormGroup.value["billing_AddressLine"],
      Add_Line1: this.patientBilling_FormGroup.value["billing_AddressLine1"],
      Add_City: this.patientBilling_FormGroup.value["billing_AddressCity"],
      Add_State: this.patientBilling_FormGroup.value["billing_AddressState"],
      Add_Zip: this.patientBilling_FormGroup.value["billing_AddressZip"],
      SSN: this.patientBilling_FormGroup.value["billing_SSN"],
      Drivers_License_FilePath: this.patientBilling_FormGroup.value["billing_DriversLicenseFilePath"],
      Primary_Phone: this.patientBilling_FormGroup.value["billing_PrimaryPhone"],
      Secondary_Phone: this.patientBilling_FormGroup.value["billing_SecondaryPhone"],
    };
    return billing;
  }

  getPatientInsurance() {
    return this.insuranceArray;
  }

  getPatientAuthorization() {
    var authorization: Authorization = {
      PatientId: this.patientId,
      Authorization_FilePath: this.patientAuthorization_FormGroup.value["authorizationFilePath"]
    };
    return authorization;
  }
  deleteRow(index) {
    this.insuranceArray.splice(index, 1);
  }

  getPatientInfoByPatientId(patientId) {
    this.patientCreateService.getPatientInfoByPatientId(patientId)
      .subscribe
      (
        res => {
          //console.log('Response:%o', res);
          this.patientId = res.sPatientDetails.patientId;
          if (res.sPatientDetails.imagePath != "") {
            this.patientimage = res.sPatientDetails.image_Path
          }
          this.setPatient(res.sPatientDetails);
          if (res.sPatientEmergency != null || res.sPatientEmergency != undefined) {
            this.setPatientEmergency(res.sPatientEmergency);
          }
          if (res.sPatientEmpContact != null || res.sPatientEmpContact != undefined) {
            this.setEmployerContact(res.sPatientEmpContact);
          }
          if (res.sPatientSocial != null || res.sPatientSocial != undefined) {
            this.setPatientSocial(res.sPatientSocial);
          }
          if (res.sPatientBilling != null || res.sPatientBilling != undefined) {
            this.setPatientBilling(res.sPatientBilling);
          }
          if (res.sPatientInsurance != null || res.sPatientInsurance != undefined) {
            this.setPatientInsurance(res.sPatientInsurance);
          }
          if (res.sPatientAuthorisation !== null) {
            this.setPatientAuthorization(res.sPatientAuthorisation);
          }
        },
        err => {
          this.toastr.errorToastr("please contact system admin!", 'Error!');
          console.log(err);
        }
      );
  }

  setPatient(sPatientDetails) {
    this.patientId = sPatientDetails.PatientId;
    this.patient_FormGroup.patchValue({
      firstName: sPatientDetails.first_Name,
      middleName: sPatientDetails.middle_Name,
      lastName: sPatientDetails.last_Name,
      DOB: sPatientDetails.dob,
      gender: sPatientDetails.gender
    });
    this.patientContact_FormGroup.patchValue({
      contact_AddressLine: sPatientDetails.add_Line,
      contact_AddressLine1: sPatientDetails.add_Line1,
      contact_AddressCity: sPatientDetails.add_City,
      contact_AddressState: sPatientDetails.add_State,
      contact_AddressPostalCode: sPatientDetails.add_Zip,
      contact_Email: sPatientDetails.email,
      contact_Active: sPatientDetails.flag,
      contact_MobNo: sPatientDetails.mob_No,
      contact_PrimaryPhone: (sPatientDetails.primary_Phone == "") ? sPatientDetails.mob_No : sPatientDetails.primary_Phone,
      contact_SecondaryPhone: sPatientDetails.secondary_Phone
    });
  }

  setPatientEmergency(sPatientEmergency) {
    this.patientEmergency_FormGroup.patchValue({
      emergency_ContactName: sPatientEmergency.contactName,
      emergency_ContactPhone: sPatientEmergency.contactPhone,
      emergency_Relationship: sPatientEmergency.relationship
    });
  }

  setEmployerContact(sPatientEmpContact) {
    this.patientEmergency_FormGroup.patchValue({
      employer_FullName: sPatientEmpContact.name,
      employer_Phone: sPatientEmpContact.phone,
      employer_Address: sPatientEmpContact.address
    });
  };

  setPatientSocial(sPatientSocial) {
    this.patientSocial1_FormGroup.patchValue({
      social_MaritalStatus: sPatientSocial.Marital_Status
    });

    this.patientSocial2_FormGroup.patchValue({
      social_GuardianFirstName: sPatientSocial.Guardian_FName,
      //social_GuardianLastName:sPatientSocial.Guardian_LName,
      social_AddressLine: sPatientSocial.Add_Line,
      social_AddressCity: sPatientSocial.Add_City,
      social_AddressState: sPatientSocial.Add_State,
      social_AddressZip: sPatientSocial.Add_Zip
    });

    //try {
    this.patientSocial3_FormGroup.patchValue({
      social_DOB: sPatientSocial.DOB,
      social_PhoneNumber: sPatientSocial.Phone_Number,
      social_GuardianSSN: sPatientSocial.Guardian_SSN
    });
    //} catch (error) { console.log('error:%o', error); }    
    /* display image --start */
    this.socialIsEdit = true;
    this.socialfileUrl = sPatientSocial.Drivers_License_FilePath;
    /* display image --end */
    this.patientSocial4_FormGroup.patchValue({
      social_PatientSSN: sPatientSocial.Patient_SSN,
      //social_DriversLicenseFilePath:sPatientSocial.Drivers_License_FilePath,
      social_Race: sPatientSocial.Race,
      social_Ethicity: sPatientSocial.Ethicity,
      social_Language: sPatientSocial.Language,
      social_CommMode: sPatientSocial.Comm_Mode,
    });

  }
  setPatientBilling(sPatientBilling) {
    /* added for image binding-- start */
    this.billingisEdit = true;
    this.billingfileUrl = sPatientBilling.Drivers_License_FilePath;
    /* added for image binding --end */

    //try {
    this.patientBilling_FormGroup.patchValue({
      billingParty: sPatientBilling.Billing_Party,
      billing_FirstName: sPatientBilling.First_Name,
      billing_MiddleName: sPatientBilling.Middle_Name,
      billing_LastName: sPatientBilling.Last_Name,
      billing_DOB: sPatientBilling.DOB,
      billing_AddressLine: sPatientBilling.Add_Line,
      billing_AddressLine1: sPatientBilling.Add_Line1,
      billing_AddressCity: sPatientBilling.Add_City,
      billing_AddressState: sPatientBilling.Add_State,
      billing_AddressZip: sPatientBilling.Add_Zip,
      billing_SSN: sPatientBilling.SSN,
      //billing_DriversLicenseFilePath: sPatientBilling.Drivers_License_FilePath,
      billing_PrimaryPhone: sPatientBilling.Primary_Phone,
      billing_SecondaryPhone: sPatientBilling.Secondary_Phone
    });
    //} catch (error) { console.log('error:%o', error); }    
  }

  setPatientInsurance(sPatientInsurance) {
    sPatientInsurance.forEach(element => {
      var insurance: Insurance = {
        PatientId: element.PatientId,
        Provider_Name: element.Provider_Name,
        Provider_Id: element.Provider_id,
        Insurance_Policy: element.Insurance_Policy,
        Policy_Type: element.Policy_Type,
        Policy_Type_Id: element.Policy_Type_ID,
        Card_Image_FilePath: element.Card_Image_FilePath,
        Effective_From: element.Effective_From,
        Status_Id: element.Status_ID,
        Status: element.Status,
        Files: this.insurancefiles[0],
      };
      this.insuranceArray.push(insurance);
    });
    return this.insuranceArray;
  }

  setPatientAuthorization(sPatientAuthorization) {
    this.authozationIsEdit = true;
    this.authorizationfileUrl = sPatientAuthorization.AuthorizationFilePath;
    this.patientAuthorization_FormGroup.patchValue({
      //authorizationFilePath: sPatientAuthorization.AuthorizationFilePath
    });
  }

  getMasterData(key) {
    this.patientCreateService.getMasterData(key)
      .subscribe
      (
        res => {
          //console.log('Response:%o', res);
          switch (key) {
            case '1':
              this.relationshipWithPatient = res;
              break;
            case '2':
              this.state = res;
              break;
            case '3':
              this.race = res;
              break;
            case '4':
              this.ethicity = res;
              break;
            case '5':
              this.preferredLanguage = res;
              break;
            case '6':
              this.preferredModeOfCommunication = res;
              break;
            case '7':
              this.providerName = res;
              break;
          }
        },
        err => {
          this.toastr.errorToastr("please contact system admin!", 'Error!');
          console.log(err);
        }
      );
  }
  formatphone(strphoneNumber: any, strcaption: any): any {
    var phoneNumber = strphoneNumber;
    phoneNumber = phoneNumber.replace(/\D+/g, '').replace(/(\d{3})(\d{3})(\d{4})/, '($1)-$2-$3');

    if (!this.validatePhoneno(phoneNumber)) {
      phoneNumber = "";
      this.toastr.errorToastr("Invalid " + strcaption + ".Phone number format must be (xxx)-xxx-xxxx", 'Oops!');
    }
    return phoneNumber;

  }
  validatePhoneno(strphoneNumber: any) {
    // /^(\()?\d{3}(\))?(-|\s)?-\d{3}-\d{4}$/;///^\d{3}-\d{3}-\d{4}$/;

    var phoneNumber = strphoneNumber;
    var phoneRGEX = /^(\()\d{3}(\))(-|\s)?-\d{3}-\d{4}$/;
    var phoneResult = phoneRGEX.test(phoneNumber);
    //alert("phone:" + phoneResult);
    return phoneResult
  }
  validateprimaryphone() {
    let phoneNumber = this.formatphone(this.patientContact_FormGroup.value["contact_PrimaryPhone"], "Primary Phone");
    this.patientContact_FormGroup.patchValue({
      contact_PrimaryPhone: phoneNumber
    })
  }
  validatesecondaryPhone() {
    let phoneNumber = this.formatphone(this.patientContact_FormGroup.value["contact_SecondaryPhone"], "Secondary Phone");
    this.patientContact_FormGroup.patchValue({
      contact_SecondaryPhone: phoneNumber
    })
  }
  validateemergencyphone() {
    let phoneNumber = this.formatphone(this.patientEmergency_FormGroup.value["emergency_ContactPhone"], "Emergency Contact Phone");
    this.patientEmergency_FormGroup.patchValue({
      emergency_ContactPhone: phoneNumber
    })
  }
  validateemployerphone() {
    let phoneNumber = this.formatphone(this.patientEmergency_FormGroup.value["employer_Phone"], "Employer Phone");
    this.patientEmergency_FormGroup.patchValue({
      employer_Phone: phoneNumber
    })
  }
  validatesocailphone() {
    let phoneNumber = this.formatphone(this.patientSocial3_FormGroup.value["social_PhoneNumber"], "Socail Phone");
    this.patientSocial3_FormGroup.patchValue({
      social_PhoneNumber: phoneNumber
    })
  }
  validatebillingPrimary() {
    let phoneNumber = this.formatphone(this.patientBilling_FormGroup.value["billing_PrimaryPhone"], "Billing Primary Phone");
    this.patientBilling_FormGroup.patchValue({
      billing_PrimaryPhone: phoneNumber
    })
  }
  validatebillingSecondary() {
    let phoneNumber = this.formatphone(this.patientBilling_FormGroup.value["billing_SecondaryPhone"], "Billing Secondary Phone");
    this.patientBilling_FormGroup.patchValue({
      billing_SecondaryPhone: phoneNumber
    })
  }
  cancel() {
    if (this.sharedService.getLocalItem("patientMode") == "edit-patient") {
      this.router.navigate(['/patient-search'], { queryParams: { mode: 'edit-patient' } });
    } else {
      this.router.navigate(['/flowsheet-book-appointment'], { queryParams: { id: this.sharedService.getLocalItem("patientMode") } });
    }
  }
}

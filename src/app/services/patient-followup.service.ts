import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SharedService } from '../core/shared.service';


const localBASE_URL="api/ChartDoc";
const epiclBASE_URL="api/ChartDoc";

@Injectable({
  providedIn: 'root'
})
export class PatientFollowupService {

  constructor(private _http: HttpClient, private sharedService: SharedService) { }

  public GetPatientFollowup(patientId: string,flag:string): Observable<any>{

    const endpoint = '/GetFollowUp/';
    
    let myHeaders = new HttpHeaders(); 
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('Accept', 'text/plain');  
    
    if(flag==='L'){
     // patientId  = btoa(patientId);
      return this._http.get(environment.baseUrl + localBASE_URL + endpoint+`${patientId}`, { headers: myHeaders});
    }
    else { // require to use epic url
    //  patientId  = btoa(patientId);
      return this._http.get(environment.baseUrl + localBASE_URL + endpoint+`${patientId}`, { headers: myHeaders});
    }
  }

  getPatientDetails(patientInfo: string){
    return this.sharedService.getLocalItem(patientInfo);
  }
  getBookingInfo(key: string){
    return this.sharedService.getLocalItem(key);
  }

  getLatestFollowUp(key: string){
    return this.sharedService.getLocalItem(key);
  }

  setFollowUp(key: string, val: any){
    this.sharedService.setLocalItem(key, val);
  }
  saveFolloUp(FollowUpData:any):Observable<any>{
    const saveEndpoint = 'api/ChartDoc/SaveFollowUp';
    //alert(encounterData);
    return this._http.post(environment.baseUrl+saveEndpoint,FollowUpData);
    //return this._http.post("http://localhost:14403/"+saveEndpoint,FollowUpData);
  }
}

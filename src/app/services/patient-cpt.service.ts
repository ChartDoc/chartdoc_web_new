import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PatientCPTModel } from '../models/PatientCPT.model';
import { environment } from 'src/environments/environment';
import { SharedService } from '../core/shared.service';


const BASE_URL = 'api/ChartDoc/GetCPT';
const baseUrl = 'api/ChartDoc/GetSavedCPT';
const localBaseUrl = 'api/ChartDoc/';

@Injectable({
  providedIn: 'root'
})
export class PatientCptService {

  patinetemployees: PatientCPTModel[];
  // tslint:disable-next-line: variable-name
  constructor(private _httpClient: HttpClient,
              private sharedService: SharedService) { }


  getPatientCptDetails(): Observable<any> {
    return this._httpClient.get(environment.baseUrl + BASE_URL);
  }


  getSavedCPT(patientId: string): Observable<any> {
   // patientId = btoa(patientId);
    return this._httpClient.get(environment.baseUrl + baseUrl + `/${patientId}`);
  }

  getPatientDetails(patientInfo: string) {
    return this.sharedService.getLocalItem(patientInfo);
  }

  getCptDetailsByPatientId(key: string) {
    return this.sharedService.getLocalItem(key);
  }

  setCptDetailsByPatientId(key: string, val: any) {
    this.sharedService.setLocalItem(key, val);
  }

  getAllCpt(key: string){
    return this.sharedService.getLocalItem(key);
  }

  getBookingInfo(key: string){
    return this.sharedService.getLocalItem(key);
  }

  setAllCpt(key: string, val: any){
    this.sharedService.setLocalItem(key, val);
  }

  saveCptDetails(cptDetailsData: PatientCPTModel[]): Observable<any> {
    return this._httpClient.post(environment.baseUrl + localBaseUrl + 'SaveCPT', cptDetailsData);

  }

}

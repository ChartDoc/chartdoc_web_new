import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PatientICDModelEmployee } from '../models/PatientICD.model';
import { SharedService } from '../core/shared.service';

const BASE_URL = 'api/ChartDoc/GetICD';
const baseUrl = 'api/ChartDoc/GetSavedICD';
//const baseUrl = 'api/ChartDoc/GetSavedICD/NzJEQkIxOUItNUMwQy00OTg5LTg4N0UtMjQ4NUJFRDI5NVBQ';

const localBaseUrl = 'api/ChartDoc/';

@Injectable({
  providedIn: 'root'
})
export class PatientIcdService {

  patinetemployees: PatientICDModelEmployee[];
  constructor(private _httpClient: HttpClient, private sharedService: SharedService) { }

  getPatientIcdDetails(): Observable<any> {
    return this._httpClient.get(environment.baseUrl + BASE_URL);
  }

  getSavedICD(patientId: string): Observable<any> {
   // patientId = btoa(patientId);
    return this._httpClient.get(environment.baseUrl + baseUrl + `/${patientId}`);
  }


  getPatientDetails(patientInfo: string) {
    return this.sharedService.getLocalItem(patientInfo);
  }
  getBookingInfo(key: string) {
    return this.sharedService.getLocalItem(key);
  }

  getIcdDetails(key: string) {
    return this.sharedService.getLocalItem(key);
  }

  setIcdDetails(key: string, val: any) {
    this.sharedService.setLocalItem(key, val);
  }

  public saveIcdDetails(icdDetailsData: PatientICDModelEmployee[]): Observable<any> {
    return this._httpClient.post(environment.baseUrl + localBaseUrl + 'SaveICD', icdDetailsData);
  }

  public getDynamicHtml(details: any){
    return this.sharedService.getDynamicHtml(details);
  }

}

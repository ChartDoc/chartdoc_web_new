import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SharedService } from '../core/shared.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
const SERVICES = "api/ChartDoc/GetServiceDetails";
const SAVESERVICE = "api/ChartDoc/SaveServiceDetails";
const DELETESERVICE = "api/ChartDoc/DeleteServiceDetails";
@Injectable({
  providedIn: 'root'
})
export class ServiceMasterService {

  constructor(private http: HttpClient, private sharedService: SharedService) { }
  public getAllServices(): Observable<any> {
    return this.http.get(environment.baseUrl + SERVICES);
  }
  public saveService(request: object): Observable<any> {
    return this.http.post(environment.baseUrl + SAVESERVICE, request
    )
  }
  public DeleteService(request:object)
  {
    return this.http.post(environment.baseUrl+DELETESERVICE,request)
  }
}

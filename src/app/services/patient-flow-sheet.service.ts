import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const BASE_URL = "api/chartdoc/GetFlowsheet/";
const SAVE_FlowSHEET = "api/chartdoc/UpdateFloorArea/";
const UPDATE_MARKIN_STATUS="api/chartdoc/UpdateMarkReady/"
const SaveCheckOut_URL = "api/chartdoc/UpdateCheckOutStatus/";

@Injectable({
  providedIn: 'root'
})
export class PatientFlowSheetService {

  constructor(private http: HttpClient) { }

  public SaveCheckOut(AppointmentID:string,ReasonCode:string):Observable<any>{    
    // [Route("api/ChartDoc/UpdateCheckOutStatus/{AppointmentID}/{ReasonCode}")]
    let test:string;
    //test = environment.baseUrl + SaveCheckOut_URL+AppointmentID+"/''";
    //http://localhost:14403/api/ChartDoc/UpdateCheckOutStatus/101/v
    return this.http.get(environment.baseUrl + SaveCheckOut_URL+AppointmentID+"/''");
  }

  public getAppointmentDetail(date: string, doctorId: string):Observable<any>{    
    return this.http.get(environment.baseUrl + BASE_URL + date + '/' + doctorId);
  }
  public updateAppoinmentDetail(AppointmentID:string,RoomNO:string,Flowarea:string):Observable<any>
  {
    return this.http.get(environment.baseUrl + SAVE_FlowSHEET+AppointmentID+"/"+RoomNO+"/"+Flowarea)
  }
  public updateMarkStatus(AppointmentID:string,Status:boolean):Observable<any>
  {
    
    return this.http.get(environment.baseUrl + UPDATE_MARKIN_STATUS+AppointmentID +"/"+ Status)

  }
}

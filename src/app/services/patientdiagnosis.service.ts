import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SharedService } from '../core/shared.service';
import { environment } from 'src/environments/environment';

// const BASE_URL='https://open-ic.epic.com/FHIR/api/FHIR/DSTU2';
//DiagnosticReport/?patient=Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB";
const document_base_url = 'api/ChartDoc/GetDocument';
const diagnosis_base_url = 'api/ChartDoc/GetDiagnosisByPatientId';
@Injectable({
  providedIn: 'root'
})
export class PatientdiagnosisService {

  constructor(private _http: HttpClient, private sharedService: SharedService) { }

  public GetPatientDiagnosis(patientId: string, flag: string): Observable<any>{
    // const endpoint = 'api/FHIR/DSTU2/DiagnosticReport';
    const epicEndPoint = 'api/FHIR/DSTU2/DiagnosticReport';
    
    
    let myHeaders = new HttpHeaders(); 
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('Accept', 'text/plain'); 

    if(flag == 'E'){
      // return this._http.get(environment.epicApiUrl +epicEndPoint, {params: new HttpParams().set('patient',patientId), headers: myHeaders});
      return this._http.get(environment.epicApiUrl +epicEndPoint + `?patient=${patientId}`, {headers: myHeaders});
    }
    else{
      patientId = btoa(patientId);
      return this._http.get(environment.baseUrl + diagnosis_base_url + `/${patientId}`, {headers: myHeaders});
    }
  }

  public GetDocumentByPatientId(patientId: string, flag: string, id: number):Observable<any>{
    let myHeaders = new HttpHeaders(); 
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('Accept', 'text/plain'); 
    
    patientId = btoa(patientId);
    return this._http.get(environment.baseUrl + document_base_url + `/${patientId}/${flag}/${id}`, {headers: myHeaders});
    
  }

  getPatientDetails(patientInfo: string){
    return this.sharedService.getLocalItem(patientInfo);
  }
  getBookingInfo(Key: string){
    return this.sharedService.getLocalItem(Key);
  }

  getDiagnosisDetails(key: string){
    return this.sharedService.getLocalItem(key);
  }

  setDiagnosisDetails(key: string, val: any){
    this.sharedService.setLocalItem(key, val);
  }

  getDocumentDetails(key: string){
    return this.sharedService.getLocalItem(key);
  }

  setDocumentDetails(key: string, val: any){
    this.sharedService.setLocalItem(key, val);
  }

  saveDiagnosisDetails(request: FormData):  Observable<any> {
    return this._http.post(environment.baseUrl + `/api/ChartDoc/SaveDiagnosis`, request);
  }
}

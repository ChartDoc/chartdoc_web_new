import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SharedService } from '../core/shared.service';
import { environment } from 'src/environments/environment';
import { observable, Observable } from 'rxjs';
import { Others } from '../models/others';
import { OtherSave } from '../models/other-save';


const SAVE_Otherts = "api/ChartDoc/SaveOthersDetails";
const Get_CategoryDetails = "api/ChartDoc/GetCategoryDetails";
const DeleteOthersDetails = "api/ChartDoc/DeleteOthersDetails";
const GetOthersDetails = "api/ChartDoc/OtherPopulate/";

@Injectable({
  providedIn: 'root'
})
export class OthersServiceService {

  constructor(private http: HttpClient, private sharedService: SharedService) { }

  public getTypes(): Observable<Others[]>{
    return this.http.get<Others[]>(environment.baseUrl + Get_CategoryDetails);
  }

  public getOthers(TypeId:string ): Observable<any>{
    return this.http.get<any>(environment.baseUrl + GetOthersDetails+TypeId);
  }
  public saveOther(request:OtherSave):Observable<any>{ 
    return this.http.post(environment.baseUrl +SAVE_Otherts,request);   
  }
  public deleteOther(OtherId:string):Observable<any>{ 
    return this.http.post(environment.baseUrl+DeleteOthersDetails,OtherId)
  }
}

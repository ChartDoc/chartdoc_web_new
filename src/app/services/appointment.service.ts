import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SharedService } from '../core/shared.service';
import { environment } from 'src/environments/environment';


const Get_DoctorList_Url="api/ChartDoc/GetDoctorList/";
const Get_Appointment_Url="api/ChartDoc/GetAppointment/";
const Get_Reasons_Url="api/ChartDoc/GetReason/";

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {

  constructor(private _http: HttpClient,
              private sharedService: SharedService) { }

  public getDoctorList(strdate: string): Observable<any>{
    const base_url = Get_DoctorList_Url;
    const querystring = strdate;

    return this._http.get(environment.baseUrl + base_url + querystring);
  }

  
  public getAllReasons(): Observable<any> {
    return this._http.get(environment.baseUrl + Get_Reasons_Url+"-3");
  }

  public getAppointment(strdate: string): Observable<any>{
    const base_url = Get_Appointment_Url;
    const querystring = strdate;

    return this._http.get(environment.baseUrl + base_url + querystring);
  }
  public setBookingInfo(key: string, val: any){
    this.sharedService.setLocalItem(key, val);
  }
  
  getBookingInfo(key: string){
    return this.sharedService.getLocalItem(key);
  }
}

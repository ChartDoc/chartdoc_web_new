import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SharedService } from '../core/shared.service';
import { environment } from 'src/environments/environment';

// const BASE_URL="api/FHIR/DSTU2";
const document_base_url = 'api/ChartDoc/GetDocument';
const Procedure_upload_Save='api/ChartDoc/SaveProcedure';
@Injectable({
  providedIn: 'root'
})
export class PatientProceduresService {

  constructor(private _http: HttpClient, private sharedService: SharedService) { }

  public GetPatientProcedures(patientId: string, flag: string): Observable<any>{

    //patientId = 'Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB'; //Required to comment out

    const epicEndpoint = 'api/FHIR/DSTU2/Procedure';
    const baseEndpoint = 'api/ChartDoc/GetProceduresByPatientId';
   
    let myHeaders = new HttpHeaders(); 
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('Accept', 'text/plain');  

    // let myParams = new HttpParams(); 
    // myParams.append('PatientID', id);
    if(flag == 'E'){
      return this._http.get(environment.epicApiUrl + epicEndpoint, {params: new HttpParams().set('patient',patientId), headers: myHeaders});
    }else{
      patientId=btoa(patientId);
      return this._http.get(environment.baseUrl + baseEndpoint + `/${patientId}`, {headers: myHeaders});
    }
    
  }

  public GetDocumentByPatientId(patientId: string, flag: string, id: number):Observable<any>{
    let myHeaders = new HttpHeaders(); 
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('Accept', 'text/plain'); 
    
    patientId = btoa(patientId);
    return this._http.get(environment.baseUrl + document_base_url + `/${patientId}/${flag}/${id}`, {headers: myHeaders});
  }

  saveProcedureDetails(request: FormData):  Observable<any> {
    return this._http.post(environment.baseUrl + Procedure_upload_Save, request);
  }

  getPatientDetails(patientInfo: string){
    return this.sharedService.getLocalItem(patientInfo);
  }

  getBookingInfo(key: string){
    return this.sharedService.getLocalItem(key);
  }

  getProcedureDetails(key: string){
    return this.sharedService.getLocalItem(key);
  }

  setProcedureDetails(key: string, val: any){
    this.sharedService.setLocalItem(key, val);
  }

  getDocumentDetails(key: string){
    return this.sharedService.getLocalItem(key);
  }

  setDocumentDetails(key: string, val: any){
    this.sharedService.setLocalItem(key, val);
  }
}

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { User } from '../models/user.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction'; // for dateClick
import resourceTimeGridPlugin from '@fullcalendar/resource-timegrid';
import { AppointmentService } from '../services/appointment.service';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { preserveWhitespacesDefault } from '@angular/compiler';

@Component({
    selector: 'app-appointment',
    templateUrl: './appointment.component.html',
    styleUrls: ['./appointment.component.css']
})
export class AppointmentComponent implements OnInit {
    isNew: boolean;
    nextListener: any;
    prevListener: any;
    doctorName: string;
    doctorImg: string;
    errorMsg: string = "";
    userName: string;
    password: string;
    errorCode: string;
    errorDesc: string;
    doctorInfo: FormGroup;
    defaultDate: any;
    @ViewChild(FullCalendarComponent, { static: false }) calendarComponent: any; // the #calendar in the template
    @ViewChild('external', { static: false }) external: ElementRef;
   
    calendarVisible = true;
    model1: string
    calendarPlugins = [timeGrigPlugin, interactionPlugin, resourceTimeGridPlugin];
    calendarWeekends = true;
    calendarEvents: EventInput[] = [];
    minDate = undefined;
    calendarresources: EventInput[] = [];
    constructor(private router: Router,
        private loginService: AuthenticationService,
        private formBuilder: FormBuilder,
        private _avRoute: ActivatedRoute,
        private _appointmentService: AppointmentService,
        private config: NgbDatepickerConfig) {
            const current = new Date();
            this.minDate = {
              year: current.getFullYear(),
              month: current.getMonth() + 1,
              day: current.getDate()
            };   
        //document.body.className = "login_page";
        let today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        let strdate = mm + '' + dd + '' + yyyy;
        this.getDoctorList(strdate);
        this.defaultDate = yyyy + '-' + mm + '-' + dd
        
       
        //var containerEl = document.getElementById('external-events-list');

    }
    ngAfterViewInit() {
        console.log(this.external.nativeElement.innerHTML);
        new Draggable(this.external.nativeElement, {
            itemSelector: '.fc-event',
            eventData: function (eventEl) {
                return {
                    title: eventEl.innerText.substring(0,1),
                    duration: '00:15:00',
                    overlap:true,
                   // textColor:'#FFFFFF',
                };
            }
        });
        
        this.bindEvents();
        
    }
    ngOnInit() {
        this.errorMsg = "";
        let today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        let strdate = mm + '' + dd + '' + yyyy;
        if (this._avRoute.snapshot.queryParams["id"]) {
           
            let lastdate= this._appointmentService.getBookingInfo("lastdate");
            this.defaultDate = lastdate.substring(6,10) + '-' + lastdate.substring(0,2)  + '-' + lastdate.substring(3,5)
            strdate=   lastdate.substring(0,2) + '' + lastdate.substring(3,5) + '' + lastdate.substring(6,10) ;
          }
        this.getAppointment(strdate);
    }

    ngOnDestroy() {
        //document.body.className = "sidebar-collapse";

    }
    
    clickCount: number = 0;
    singleClickTimer: any
    rowclick(arg: any) {
        debugger;
        this.clickCount++;
        

        let self=this;
        if (this.clickCount === 1) {
            this.singleClickTimer = setTimeout(function () {
                console.log('single click');
                self.clickCount = 0;
            }, 400);
        } else if (this.clickCount === 2) {
            console.log('double click');

            this.clickCount = 0;
            clearTimeout(this.singleClickTimer);
            if(arg.event.backgroundColor=="#ffc4c4" || arg.event.backgroundColor=="#ffffd8"){
                   console.log(arg.event.backgroundColor) 
            }else{
                this.clickCount++;        
                let doctorid=arg.event.getResources()[0].id;
                let doctorname=arg.event.getResources()[0].title;
        
                let startdate = new Date(arg.event.start);
                var dd = String(startdate.getDate()).padStart(2, '0');
                var mm = String(startdate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = startdate.getFullYear();
                //let strdate = dd+'/'+mm+'/'+yyyy; //mm + '' + dd + '' + yyyy;
                let strdate = mm + '/' + dd + '/' + yyyy; //mm + '' + dd + '' + yyyy;
                let starttime=String(startdate.getHours())+":"+String(startdate.getMinutes());
        
                let enddate = new Date(arg.event.end);
                dd = String(enddate.getDate()).padStart(2, '0');
                mm = String(enddate.getMonth() + 1).padStart(2, '0'); //January is 0!
                yyyy = enddate.getFullYear();
                let enddt = mm + '' + dd + '' + yyyy;
                let endtime=String(enddate.getHours())+":"+String(enddate.getMinutes());
                let appointmentId="0";
                let patientName="";
                if(arg.event.extendedProps.patientName==undefined){
                    let temp = arg.el.innerText.split('/');
                    if(temp.length>1){
                    patientName=temp[0];
                    appointmentId=temp[1];
                    }
                }else{
                    patientName=arg.event.extendedProps.patientName;//temp[0];
                    appointmentId=arg.event.extendedProps.appointmentId; 
                }
                let patientDetail = this.calendarEvents.filter(x => x.appointmentId == appointmentId && x.patientName ==  patientName)[0];
                this._appointmentService.setBookingInfo("doctorBookingInfo",{"doctorid":doctorid,
                "doctorname":doctorname,"startdate":strdate,"enddate":enddt,"starttime":starttime,
                "endtime":endtime,"patientname": patientName,
                "patientId": (patientDetail!=undefined || patientDetail!=null) ?patientDetail.patientID:"",
                "appointmentid": appointmentId,"email":(patientDetail!=undefined || patientDetail!=null) ?patientDetail.email:"",
                "phone":(patientDetail!=undefined || patientDetail!=null) ?patientDetail.phone:"",
                "dateOfBirth":(patientDetail!=undefined || patientDetail!=null) ?patientDetail.dateOfBirth:"",
                "gender":(patientDetail!=undefined || patientDetail!=null) ?patientDetail.gender:"",
                "address":(patientDetail!=undefined || patientDetail!=null) ?patientDetail.address:"",
                "serviceID":((patientDetail!=undefined || patientDetail!=null) && patientDetail.serviceID!="") ?patientDetail.serviceID:"",
                "positionID":((patientDetail!=undefined || patientDetail!=null) && patientDetail.positionID!="") ?patientDetail.positionID:"0",
                "reasonID":((patientDetail!=undefined || patientDetail!=null) && patientDetail.reasonID!="")  ?patientDetail.reasonID:"0",
                "reasonCode":"0",
                "note":(patientDetail!=undefined || patientDetail!=null) ?patientDetail.note:""
                });
        
                this.router.navigateByUrl('/book-appointment');
            }
        }


    }
    
    eventDrop(arg:any){
        
       console.log(arg)
    }
    
    drop(arg:any){
       
       console.log(arg)
    }
    datechange(arg: any) {
        this.external.nativeElement.style="display: visible;";

        let strdate =  String(arg.month).padStart(2, '0')+""+ String(arg.day).padStart(2, '0')+""+arg.year;
        this.defaultDate = arg.year+"-"+String(arg.month).padStart(2, '0')+"-"+ String(arg.day).padStart(2, '0');
       
        this.calendarComponent.calendar.gotoDate(this.defaultDate);
        this.getDoctorList(strdate);
        this.getAppointment(strdate);

    }

    getDoctorList(strdate: any) {
        this._appointmentService.getDoctorList(strdate)
            .subscribe((res) => {
                
                let tempdata = [];
                res.forEach(element => {

                    tempdata.push({ id: element.Id, title: element.Name })

                });
                this.calendarresources = tempdata;
            },
                err => {
                    console.log(err);
                });
    }
    getAppointment(strdate: any) {
        let self=this;
        this._appointmentService.getAppointment(strdate)
            .subscribe((res) => {

                let tempdata = [];
                let calendarApi = self.calendarComponent.getApi();
                let nextdaydate = new Date(calendarApi.getDate())
                var dd = String(nextdaydate.getDate()).padStart(2, '0');
                var mm = String(nextdaydate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = nextdaydate.getFullYear();
                let strdate1 = yyyy + '-' + mm + '-' + dd;

                res.forEach((element: any) => {
                    if (element.schduleAppoinment.DoctorID != '') {
                        let AppointmentId=String(element.schduleAppoinment.AppointmentId)!="0"?"/"+String(element.schduleAppoinment.AppointmentId):"";
                        tempdata.push({
                            resourceId: element.schduleAppoinment.DoctorID, title: element.schduleAppoinment.PatientName+AppointmentId, start: strdate1 + "T" + element.schduleAppoinment.FromTime,
                            end: strdate1 + "T" + element.schduleAppoinment.ToTime,patientName:element.schduleAppoinment.PatientName,appointmentId:element.schduleAppoinment.AppointmentId, 
                            patientID: element.schduleAppoinment.PatientID,email:element.schduleAppoinment.Email,phone:element.schduleAppoinment.Phoneno
                            ,serviceID: element.schduleAppoinment.ServiceID,positionID: element.schduleAppoinment.PositionID
                            ,note: element.schduleAppoinment.Note,reasonID: element.schduleAppoinment.ReasonID
                            ,dateOfBirth:element.schduleAppoinment.DateofBirth,gender:element.schduleAppoinment.Gender,address:element.schduleAppoinment.Address,color:element.schduleAppoinment.ColorCode.trim(),
                            
                            editable:
                            (element.schduleAppoinment.ColorCode.trim()=='#ffc4c4'
                            || element.schduleAppoinment.ColorCode.trim()=='#ffffd8'
                            || element.schduleAppoinment.ColorCode.trim()=='#d8d8ff')
                            ?false:true,
                            resourceEditable: (element.schduleAppoinment.ColorCode.trim()=='#ffc4c4'
                            || element.schduleAppoinment.ColorCode.trim()=='#ffffd8'
                            || element.schduleAppoinment.ColorCode.trim()=='#d8d8ff')
                           ?false:true,
                           overlap:
                           (element.schduleAppoinment.ColorCode.trim()=='#ffc4c4'
                           || element.schduleAppoinment.ColorCode.trim()=='#ffffd8')
                           ?false:true,
                           //textColor:'#FFFFFF',
                           
                            

                        });
                    }
                });
                
                this.calendarEvents = tempdata;
            },
                err => {
                    console.log(err);
                });
    }
    bindEvents() {

        let prevButton = this.calendarComponent.element.nativeElement.getElementsByClassName("fc-prev-button");
        let nextButton = this.calendarComponent.element.nativeElement.getElementsByClassName("fc-next-button");
        
        let todayButton = this.calendarComponent.element.nativeElement.getElementsByClassName("fc-today-button");
        nextButton[0].addEventListener('click', () => {

            let calendarApi = this.calendarComponent.getApi();
            let nextdaydate = new Date(calendarApi.getDate())
            var dd = String(nextdaydate.getDate()).padStart(2, '0');
            var mm = String(nextdaydate.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = nextdaydate.getFullYear();
            let strdate = mm + '' + dd + '' + yyyy;
            this.getDoctorList(strdate);
            this.getAppointment(strdate);

            console.log("nextClick")
            let today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();
            let strdate1 = mm + '' + dd + '' + yyyy;
            if(Number(strdate)<Number(strdate1)){
                this.external.nativeElement.style="display: none;";
            }
            else{
                this.external.nativeElement.style="display: visible;";
            }
        });
        prevButton[0].addEventListener('click', () => {
           
            let calendarApi = this.calendarComponent.getApi();
            let prevdaydate = new Date(calendarApi.getDate())
            var dd = String(prevdaydate.getDate()).padStart(2, '0');
            var mm = String(prevdaydate.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = prevdaydate.getFullYear();
            let strdate = mm + '' + dd + '' + yyyy;
            this.getDoctorList(strdate);
            this.getAppointment(strdate);
            console.log("prevClick");
            let today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();
            let strdate1 = mm + '' + dd + '' + yyyy;
            this.external.nativeElement.style="display: visible;";
            if(Number(strdate)<Number(strdate1)){
                this.external.nativeElement.style="display: none;";
            }
           
        });

        todayButton[0].addEventListener('click', () => {
            let calendarApi = this.calendarComponent.getApi();
            let todaydate = new Date(calendarApi.getDate())
            var dd = String(todaydate.getDate()).padStart(2, '0');
            var mm = String(todaydate.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = todaydate.getFullYear();
            let strdate = mm + '' + dd + '' + yyyy;
            this.getDoctorList(strdate);
            this.getAppointment(strdate);
            console.log("todayClick")
            this.external.nativeElement.style="display: visible;";
        });
    }
    dateClick(info) {
        //alert('Date: ' + info.dateStr);
        info.dayEl.style.backgroundColor = 'red';
        alert('Resource ID: ' + info.resource.id);
      }

}

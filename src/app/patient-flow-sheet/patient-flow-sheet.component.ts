import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput } from '@fullcalendar/core';
import { PatientFlowSheetService } from '../services/patient-flow-sheet.service';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction'; // for dateClick
import resourceTimeGridPlugin from '@fullcalendar/resource-timegrid';
import { Router } from '@angular/router';
import { parse } from 'querystring';
import { AppointmentService } from '../services/appointment.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-patient-flow-sheet',
  templateUrl: './patient-flow-sheet.component.html',
  styleUrls: ['./patient-flow-sheet.component.css']
})
export class PatientFlowSheetComponent implements OnInit {
  @ViewChild(FullCalendarComponent, { static: false }) calendarComponent: any; // the #calendar in the template
  // @ViewChild('external', { static: false }) external: ElementRef;
  isCheckinValid:boolean=false;
  calendarVisible = true;
  //calendarPlugins = [timeGrigPlugin, interactionPlugin, resourceTimeGridPlugin];
  calendarEvents: EventInput[] = [];
  calendarresources: EventInput[] = [];
  appoinmentId: string;
  roomNumber: string;
  SavedRoom:string;
  FlowArea: string;
  defaultDate: any;
  markStatus: boolean;
  error: boolean;
  currentDate: string;

  //tempdata = [];
  calendarPlugins = [timeGrigPlugin, interactionPlugin, resourceTimeGridPlugin];
  patientData: [];
  doctorId: string;
  isCurrentDateApp:boolean=true;

  constructor(private _patientFlowSheetService: PatientFlowSheetService, private _appointmentService: AppointmentService,
    private router: Router,public toastr: ToastrManager, private loginService: AuthenticationService) {
  }

  getAppointmentDetails(strDate: any, doctorId: string) {
    //alert("start date="+strDate);
    this._patientFlowSheetService.getAppointmentDetail(strDate, doctorId)
      .subscribe((res) => {
        this.patientData = res;
        console.log("data=" + JSON.stringify(this.patientData))
        let tempdata = [];

        let calendarApi = this.calendarComponent.getApi();
        let nextdaydate = new Date(calendarApi.getDate())
        var dd = String(nextdaydate.getDate()).padStart(2, '0');
        var mm = String(nextdaydate.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = nextdaydate.getFullYear();
        let strdate1 = yyyy + '-' + mm + '-' + dd;
        this.defaultDate = yyyy + '-' + mm + '-' + dd

        res.forEach((element: any) => {
          if (element.schduleAppoinment.doctorID != "") {
            tempdata.push({
              resourceId: 1, title: element.schduleAppoinment.patientName.trim()+" ("+element.schduleAppoinment.appointmentFromTime.trim()+"-"+element.schduleAppoinment.appointmentToTime.trim()+")", start: strdate1 + "T" + element.schduleAppoinment.fromTime.trim(),
              end: strdate1 + "T" + element.schduleAppoinment.toTime.trim(), IsReady: element.schduleAppoinment.isReady, appoinmentId: element.schduleAppoinment.appointmentId, patientID: element.schduleAppoinment.patientID,
              email: element.schduleAppoinment.email, phone: element.schduleAppoinment.phoneno,patientName:element.schduleAppoinment.patientName,doctorName:element.schduleAppoinment.doctorName
              , dateOfBirth: element.schduleAppoinment.dateofBirth, gender: element.schduleAppoinment.gender, address: element.schduleAppoinment.address,serviceId:element.schduleAppoinment.serviceID,note:element.schduleAppoinment.note,reason:element.schduleAppoinment.reason,positionID:element.schduleAppoinment.positionID,reasonID:element.schduleAppoinment.reasonID
              ,color: element.schduleAppoinment.isReady == "True" ? 'yellow' : element.schduleAppoinment.colorCode.trim(), HeaderId: 1,appointmentFromTime:strdate1 + "T"+element.schduleAppoinment.appointmentFromTime.trim(),appointmentToTime:strdate1 + "T"+element.schduleAppoinment.appointmentToTime.trim(),roomNo:element.schduleAppoinment.roomno,
              imageUrl:element.schduleAppoinment.imageUrl
              // editable:
            });
          }
          else if (element.waitingArea.doctorID != "") {
            tempdata.push({
              resourceId: 2, title: element.waitingArea.patientName.trim() +" ("+element.waitingArea.appointmentFromTime.trim()+"-"+element.waitingArea.appointmentToTime.trim()+")", start: strdate1 + "T" + element.waitingArea.fromTime,
              end: strdate1 + "T" + element.waitingArea.toTime, appoinmentId: element.waitingArea.appointmentId, patientID: element.waitingArea.patientID, HeaderId: 2, email: element.waitingArea.email, phone: element.waitingArea.phoneno
              , dateOfBirth: element.waitingArea.dateofBirth, gender: element.waitingArea.gender, address: element.waitingArea.address,patientName:element.waitingArea.patientName,doctorName:element.waitingArea.doctorName,
              serviceId:element.waitingArea.serviceID,note:element.waitingArea.note,reason:element.waitingArea.reason,positionID:element.waitingArea.positionID,reasonID:element.waitingArea.reasonID,appointmentFromTime:strdate1 + "T"+element.waitingArea.appointmentFromTime.trim(),appointmentToTime:strdate1 + "T"+element.waitingArea.appointmentToTime.trim(),roomNo:element.waitingArea.roomno,
              imageUrl:element.waitingArea.imageUrl


            });
          }
          else if (element.consultationRoom.DoctorID != "") {            
            this.SavedRoom = this._appointmentService.getBookingInfo("SavedRoom"+element.consultationRoom.appointmentId);
            if(!this.SavedRoom)
            this.SavedRoom="";
            
            tempdata.push({
              resourceId: 3, title: element.consultationRoom.patientName +" ("+element.consultationRoom.appointmentFromTime.trim()+"-"+element.consultationRoom.appointmentToTime.trim()+")"+"(Room:"+this.SavedRoom+")",
              start: strdate1 + "T" + element.consultationRoom.fromTime,
              end: strdate1 + "T" + element.consultationRoom.toTime, appoinmentId: element.consultationRoom.appointmentId, patientID: element.consultationRoom.patientID, HeaderId: 3,
              email: element.consultationRoom.email, phone: element.consultationRoom.phoneno,patientName:element.consultationRoom.patientName,doctorName:element.consultationRoom.doctorName,
              dateOfBirth: element.consultationRoom.dateofBirth, gender: element.consultationRoom.gender, address: element.consultationRoom.address,roomNo:element.consultationRoom.roomno,
              serviceId:element.consultationRoom.serviceID,note:element.consultationRoom.note,reason:element.consultationRoom.reason,positionID:element.consultationRoom.positionID,reasonID:element.consultationRoom.reasonID,appointmentFromTime:strdate1 + "T"+element.consultationRoom.appointmentFromTime.trim(),appointmentToTime:strdate1 + "T"+element.consultationRoom.appointmentToTime.trim(),
              imageUrl:element.consultationRoom.imageUrl

            });
          }
          else if (element.checkingOut.doctorID != "") {
            tempdata.push({
              resourceId: 4, title: element.checkingOut.patientName +" ("+element.checkingOut.appointmentFromTime.trim()+"-"+element.checkingOut.appointmentToTime.trim()+")", start: strdate1 + "T" + element.checkingOut.fromTime,
              end: strdate1 + "T" + element.checkingOut.toTime, appoinmentId: element.checkingOut.appointmentId, patientID: element.checkingOut.patientID, HeaderId: 4,
              email: element.checkingOut.email, phone: element.checkingOut.phoneno,patientName:element.checkingOut.patientName,doctorName:element.checkingOut.doctorName,
               dateOfBirth: element.checkingOut.dateofBirth, gender: element.checkingOut.gender, address: element.checkingOut.address,
               serviceId:element.checkingOut.serviceID,note:element.checkingOut.note,reason:element.checkingOut.reason,positionID:element.checkingOut.positionID,reasonID:element.checkingOut.reasonID,appointmentFromTime:strdate1 + "T"+element.checkingOut.appointmentFromTime,appointmentToTime:strdate1 + "T"+element.checkingOut.appointmentToTime.trim(),roomNo:element.checkingOut.roomno,
               imageUrl:element.checkingOut.imageUrl

            });
          }
        });
        this.calendarEvents = tempdata;
      },
        err => {
          console.log(err);
        });
  }

  ngOnInit() {
    this.isCurrentDateApp=true;
    this.doctorId = this.loginService.getLocalStorage('doctorInfo').doctorId;
    let today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    let strdate = mm + '' + dd + '' + yyyy;
    this.currentDate = mm + '' + dd + yyyy;
    let header = [];

    header.push({ id: 1, title: "Scheduled Appointments" });
    header.push({ id: 2, title: "Waiting Area" });
    header.push({ id: 3, title: "Consultation Room" });
    header.push({ id: 4, title: "Checking out" });

    this.calendarresources = header;

    this.getAppointmentDetails(strdate, this.doctorId);
  }
  hideTimeCol() {
    this.calendarComponent.element.nativeElement.getElementsByClassName("fc-axis fc-widget-header")[0].style.display = 'none';
    let timecell = this.calendarComponent.element.nativeElement.getElementsByClassName("fc-axis fc-time");
   
    for (let i = 0; i < timecell.length; i++) {
      timecell[i].style.display = 'none';
      //timecell1[i].style.display = 'none';
    }
    //this.calendarComponent.element.nativeElement.getElementsByClassName("fc-time")[0].style.display = 'none';
  }
  ngAfterViewInit() {
    this.hideTimeCol();
    this.bindEvents();
  }
  dragdate(arg: any) {
    this.roomNumber = "0";
    this.appoinmentId = arg.oldEvent._def.extendedProps.appoinmentId;
    if (parseInt(arg.newResource._resource.id) == 1) {
      this.FlowArea = "Appointment";
    }
    else if (parseInt(arg.newResource._resource.id) == 2) {
      this.FlowArea = "Waiting";
    }
    else if (parseInt(arg.newResource._resource.id) == 3) {
      this.FlowArea = "Encounter";
    }
    else if (parseInt(arg.newResource._resource.id) == 4) {
      this.FlowArea = "Finish";

    }

    let today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    let strdate = mm + '' + dd + '' + yyyy;

    let calendarApi = this.calendarComponent.getApi();
    let prevdaydate = new Date(calendarApi.getDate())
    var dd = String(prevdaydate.getDate()).padStart(2, '0');
    var mm = String(prevdaydate.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = prevdaydate.getFullYear();
    let Appointdate = mm + '' + dd + '' + yyyy;

    if(strdate!=Appointdate){
      this.toastr.warningToastr("Event movement allowed for Current date only.");
      arg.revert();
    }
    else{
        if ((parseInt(arg.oldResource._resource.id)) == 1) {
          if (arg.oldEvent._def.extendedProps.IsReady == "True") {
              if (parseInt(arg.newResource._resource.id) == 3) {
                this.roomNumber=arg.oldEvent._def.extendedProps.roomNo;
                this.openRoomDialog('modaladdRoom');
              }
              else {
                this.updatePatientDetail("0");
                //arg.revert();
              }
          }
          else {
            arg.revert();
          }
        }
      // else if ((parseInt(arg.oldResource._resource.id)) == 2 && parseInt(arg.newResource._resource.id)<(parseInt(arg.oldResource._resource.id))) {
        else if(parseInt(arg.newResource._resource.id) == 3)
        {
          this.roomNumber=arg.oldEvent._def.extendedProps.roomNo;
          this.openRoomDialog('modaladdRoom');
        }
        else {
        //  arg.revert();
        this.updatePatientDetail("0");
        }
    }

  }
  clickCount: number = 0;
  singleClickTimer: any
  rowclick(arg: any) {
    this.appoinmentId = arg.event.extendedProps.appoinmentId;
    let HeaderId=arg.event.extendedProps.HeaderId;
    this.clickCount++;
    let self = this;
    if (this.clickCount === 1) {
      if(this.isCurrentDateApp){
          this.singleClickTimer = setTimeout(function () {
            console.log('single click');
            if (HeaderId == 1) {
              self.markStatus = arg.event.extendedProps.isReady == "True" ? true : false;
              self.openMarkIn("modalmarkInPatient")
            }
            if(HeaderId ==4){
              // self.markStatus = arg.event.extendedProps.IsReady == "True" ? true : false;
              self.openCheckin('modalCheckOut');
            }        
            if(HeaderId ==3){
              self.openRoomUpdate('modaladdRoom');
            }
            self.clickCount = 0;
          }, 400);
    }else{
      //this.toastr.warningToastr("MarkIn Patielnts allowed for Current date only.");
      this.singleClickTimer = setTimeout(function () {
        self.clickCount = 0;
      }, 400);      
    }
    } else if (this.clickCount === 2) {
      console.log('double click');
      this.clickCount = 0;
      clearTimeout(this.singleClickTimer);
      this.clickCount++;
    
      let startdate = new Date(arg.event.extendedProps.appointmentFromTime);
      var dd = String(startdate.getDate()).padStart(2, '0');
      var mm = String(startdate.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = startdate.getFullYear();
      //let strdate = dd+'/'+mm+'/'+yyyy; //mm + '' + dd + '' + yyyy;
      let strdate = mm + '/' + dd + '/' + yyyy; //mm + '' + dd + '' + yyyy;
      let starttime = String(startdate.getHours()) + ":" + String(startdate.getMinutes());

      let enddate = new Date(arg.event.extendedProps.appointmentToTime);
      dd = String(enddate.getDate()).padStart(2, '0');
      mm = String(enddate.getMonth() + 1).padStart(2, '0'); //January is 0!
      yyyy = enddate.getFullYear();
      let enddt = mm + '' + dd + '' + yyyy;
      let endtime = String(enddate.getHours()) + ":" + String(enddate.getMinutes());
      let temp = arg.el.innerText;
      //let appointmentId = "0";
      let patientName = "";
      patientName=temp;
      this._appointmentService.setBookingInfo("doctorBookingInfo", {
        "doctorid": arg.event.extendedProps.doctorID,
        "doctorname": arg.event.extendedProps.doctorName, "startdate": strdate, "enddate": enddt, "starttime": starttime,
        "endtime": endtime, "patientname": patientName,
        "patientId": (arg.event.extendedProps != undefined ) ? arg.event.extendedProps.patientID : "",
        "appointmentid": this.appoinmentId, "email": (arg.event.extendedProps!= undefined ) ? arg.event.extendedProps.email : "",
        "phone": (arg.event.extendedProps.phone != undefined ) ? arg.event.extendedProps.phone : "",
        "dateOfBirth": (arg.event.extendedProps.dateOfBirth != undefined) ? arg.event.extendedProps.dateOfBirth : "",
        "gender": (arg.event.extendedProps != undefined ) ? arg.event.extendedProps.gender : "",
        "address": (arg.event.extendedProps != undefined ) ? arg.event.extendedProps.address: "",
        "serviceID":(arg.event.extendedProps!=undefined)? arg.event.extendedProps.serviceId:"",
        "note":(arg.event.extendedProps!=undefined)? arg.event.extendedProps.note:"",
        "reasonCode":"0",
        // "reason":(arg.event.extendedProps!=undefined)? arg.event.extendedProps.reason:"",
        "reasonID":(arg.event.extendedProps!=undefined)? arg.event.extendedProps.reasonID:"",
        "positionID":(arg.event.extendedProps!=undefined)? arg.event.extendedProps.positionID:"",
        "imageUrl":(arg.event.extendedProps!=undefined)? arg.event.extendedProps.imageUrl:"",
       
      });
    //  alert("doctorBookingInfo=" + JSON.stringify(this._appointmentService.getBookingInfo("doctorBookingInfo")))
      this.router.navigate(['/flowsheet-book-appointment'],{queryParams:{id:arg.event.extendedProps.HeaderId}});


    }
  }

  public openMarkIn(myModal: string) {
    this.error = false;
    document.getElementById(myModal).style.display = "block";
  }

  public openCheckin(myModal: string) {
    // this.error = false;
    document.getElementById(myModal).style.display = "block";
  }

  public openRoomUpdate(myModal: string) {
    this.roomNumber = this._appointmentService.getBookingInfo("SavedRoom"+this.appoinmentId);
    document.getElementById(myModal).style.display = "block";
  }

  public closePopuop(myModal: string,roomNo: string) {    
    if(myModal=="modaladdRoom" && (roomNo || roomNo == "")){
      if(this.SavedRoom!=roomNo && roomNo=="undefined")
         roomNo=this.SavedRoom;
       this.updatePatientDetail(roomNo);
     }
     else{
      document.getElementById(myModal).style.display = "none";   
      this.getAppointmentDetails(this.currentDate, this.doctorId);
    }
    
  }
  
  public closeRoomPopuop(myModal: string){
    document.getElementById(myModal).style.display = "none";
  }

  public handleError() {
    this.error = false;
  }

  public submitMarkIn(status: boolean) {
    if (status == undefined) {
      this.error = true;
      return false;
    }
    this._patientFlowSheetService.updateMarkStatus(this.appoinmentId, status).subscribe((res) => {
      if (res == 1) {
        console.log("success")
        this.getAppointmentDetails(this.currentDate, this.doctorId);
      }
      else {
        console.log("Fail");
      }
    }, err => {
      this.closePopuop('modalmarkInPatient','');
      console.log(err);
    })
    this.closePopuop('modalmarkInPatient','');
  }
  public submitRoom(roomNo: string) {
    // this.SavedRoom=roomNo;
    this.FlowArea ="Encounter";
    this._appointmentService.setBookingInfo("SavedRoom"+this.appoinmentId,roomNo);

    this.updatePatientDetail(roomNo);
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  public openRoomDialog(myModel: string) {
    this.error = false;
    this.roomNumber = this._appointmentService.getBookingInfo("SavedRoom"+this.appoinmentId);
    document.getElementById(myModel).style.display = "block";
  }

  updatePatientDetail(roomNo: string) {
    if(roomNo=="")
      roomNo="0";
    this._patientFlowSheetService.updateAppoinmentDetail(this.appoinmentId, roomNo, this.FlowArea).subscribe((res) => {
      console.log("status=" + JSON.stringify(res))
      this.getAppointmentDetails(this.currentDate, this.doctorId);
      // this.closePopuop('modaladdRoom','');
      this.closeRoomPopuop('modaladdRoom');
    }, err => {
      this.closeRoomPopuop('modaladdRoom');
      console.log(err);
    })
  }
  toggleVisibility(e){
    this.isCheckinValid= e.target.checked;
  }

  submitmodalCheckOut(){  
    this._patientFlowSheetService.SaveCheckOut(this.appoinmentId,"").subscribe((res) => {
      this.toastr.successToastr("Fee Ticket Number: "+res);
      this.closePopuop('modalCheckOut','');
      this.isCheckinValid=false;
    }, err => {
      console.log(err);
    })   
  }

  bindEvents() {
    let prevButton = this.calendarComponent.element.nativeElement.getElementsByClassName("fc-prev-button");
    let nextButton = this.calendarComponent.element.nativeElement.getElementsByClassName("fc-next-button");
    let todayButton = this.calendarComponent.element.nativeElement.getElementsByClassName("fc-today-button");

    nextButton[0].addEventListener('click', () => {
      this.hideTimeCol();
      let calendarApi = this.calendarComponent.getApi();
      let nextdaydate = new Date(calendarApi.getDate())
      var dd = String(nextdaydate.getDate()).padStart(2, '0');
      var mm = String(nextdaydate.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = nextdaydate.getFullYear();
      let strdate = mm + '' + dd + '' + yyyy;
      this.currentDate = strdate;
      this.getAppointmentDetails(strdate, this.doctorId);
      console.log("nextClick")
      this.IsCurrentAppointment();
    });
    prevButton[0].addEventListener('click', () => {
      this.hideTimeCol();
      let calendarApi = this.calendarComponent.getApi();
      let prevdaydate = new Date(calendarApi.getDate())
      var dd = String(prevdaydate.getDate()).padStart(2, '0');
      var mm = String(prevdaydate.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = prevdaydate.getFullYear();
      let strdate = mm + '' + dd + '' + yyyy;
      this.currentDate = strdate;
      this.getAppointmentDetails(strdate, this.doctorId);
      console.log("prevClick")
      this.IsCurrentAppointment();
    });

    todayButton[0].addEventListener('click', () => {
      this.hideTimeCol();
      let calendarApi = this.calendarComponent.getApi();
      let todaydate = new Date(calendarApi.getDate())
      var dd = String(todaydate.getDate()).padStart(2, '0');
      var mm = String(todaydate.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = todaydate.getFullYear();
      let strdate = mm + '' + dd + '' + yyyy;
      this.currentDate = strdate;
      this.getAppointmentDetails(strdate, this.doctorId);
      console.log("todayClick")
    });
  }

  IsCurrentAppointment(){
    let today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    let strdate = mm + '' + dd + '' + yyyy;

    let calendarApi = this.calendarComponent.getApi();
    let prevdaydate = new Date(calendarApi.getDate())
    var dd = String(prevdaydate.getDate()).padStart(2, '0');
    var mm = String(prevdaydate.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = prevdaydate.getFullYear();
    let Appointdate = mm + '' + dd + '' + yyyy;

    if(strdate!=Appointdate)
      this.isCurrentDateApp=false;
    else
      this.isCurrentDateApp=true;
  }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import {Alert, Allergies, Immunizations,Socials,Families} from './patient-others-model';
import { SharedService } from 'src/app/core/shared.service';
import { PatientCreateService } from '../services/patient-create.service';
import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-patient-others',
  templateUrl: './patient-others.component.html',
  styleUrls: ['./patient-others.component.css']
})
export class PatientOthersComponent implements OnInit {

  allergyArray: Array<Allergies> = [];
  immunizationArray: Array<Immunizations> = [];
  socailArray: Array<Socials> = [];
  familyArray: Array<Families> = [];
  alertKeywordArray=[];
  patientId ='0';
  patientimage:any="noimage.png";
  ClsAllergyImmunization = {
    PatientID: '0',
    Allergies: [],
    Immunizations: [],
    Socials:[],
    Alert: {},
    Families:[],

  }
  patient_FormGroup = new FormGroup({
    firstName: new FormControl(''),
    middleName:new FormControl(''),
    lastName: new FormControl(''),
    gender: new FormControl(''),
    DOB: new FormControl('')
  });

  allergies_FormGroup=new FormGroup({
    allergies_Description: new FormControl(''),
    allergies_Code: new FormControl(''),
  });

  alert_FormGroup=new FormGroup({
    alert_Description: new FormControl(''),
    alert_Code: new FormControl(''),
  });

  immunizations_FormGroup=new FormGroup({
    immunizations_Date: new FormControl(''),
    immunizations_Code: new FormControl(''),
    immunizations_Description: new FormControl(''),
  });

  social_FormGroup=new FormGroup({
    social_Addiection: new FormControl(''),
    social_Frequency: new FormControl(''),
    social_Duration: new FormControl('')
  });
  family_FormGroup=new FormGroup({
    family_Member: new FormControl(''),
    family_Diseases: new FormControl('')
  });
  constructor(private patientCreateService: PatientCreateService, 
    private router: Router, private sharedService: SharedService
    , public toastr: ToastrManager) { }

  ngOnInit() {
    var patientDetail = this.sharedService.getLocalItem("patientDetail");
    if(patientDetail!==null){
      this.patientId =patientDetail.PatientId;
       this.patientimage=patientDetail.ImagePath;
      this.patient_FormGroup.patchValue({
        firstName:patientDetail.FirstName,
        middleName:patientDetail.MiddleName,
        lastName:patientDetail.LastName,
        DOB:patientDetail.DOB,
        gender:patientDetail.Gender,
        
      });

      this.patientCreateService.getImmunizations(this.patientId)
        .subscribe((res) => {
          res.forEach(element => {
          this.immunizationArray.push(
          {
            Code:element.Code,
            Date:element.Date.substring(3,5)+"-"+element.Date.substring(0,2)+"-"+element.Date.substring(6,10),
            Description:element.Description,
            PatientId:element.PatientId,
            ID:element.ID
          });
        });
            //  console.log(this.immunizationArray);   
      },
      err => {
        console.log(err);
      });

        this.patientCreateService.getAllergies(this.patientId)
        .subscribe((res) => {
          res.forEach(element => {
          this.allergyArray.push(
          {
            Code:element.Code,
            Description:element.Description,
            PatientId:element.PatientId,
            ID:element.ID
          });
        });
            //  console.log(this.allergyArray);   
      },
      err => {
        console.log(err);
      });

      this.patientCreateService.getSocials(this.patientId)
        .subscribe((res) => {
          res.forEach(element => {
          this.socailArray.push(
          {
            Addiection:element.Addiection,
            Frequency:element.Frequency,
            Duration:element.Duration,
            PatientID:element.PatientID,
            ID:element.ID
          });
        });
            //  console.log(this.allergyArray);   
      },
      err => {
        console.log(err);
      });

      this.patientCreateService.getFamilies(this.patientId)
      .subscribe((res) => {
        res.forEach(element => {
        this.familyArray.push(
        {
          Member:element.Member,
          Diseases:element.Diseases,
          PatientID:element.PatientID,
          ID:element.ID
        });
      });
          //  console.log(this.allergyArray);   
    },
    err => {
      console.log(err);
    });
      
    }
    this.getMasterData('8');
  }
  finish() {
    this.ClsAllergyImmunization.PatientID = this.patientId;
    this.ClsAllergyImmunization.Immunizations = this.getImmunizations();
    this.ClsAllergyImmunization.Allergies = this.getAllergies();
    this.ClsAllergyImmunization.Socials= this.getSocial();
    this.ClsAllergyImmunization.Families= this. getFamily();
    this.ClsAllergyImmunization.Alert = this.getAlert();
    if(this.ClsAllergyImmunization.Immunizations.length==0){
      this.toastr.errorToastr("Add Immunizations", 'Oops!');
      return;
    }
    if(this.ClsAllergyImmunization.Allergies.length==0){
      this.toastr.errorToastr("Add Allergies", 'Oops!');
      return;
    }
    if(this.ClsAllergyImmunization.Socials.length==0){
      this.toastr.errorToastr("Add Social", 'Oops!');
      return;
    }
    if(this.ClsAllergyImmunization.Families.length==0){
      this.toastr.errorToastr("Add Family", 'Oops!');
      return;
    }

    this.patientCreateService.patientOtherService(this.ClsAllergyImmunization)
      .subscribe
      (
          res => {
            this.sharedService.removeLocalStorage("patientDetail");
            if(this.sharedService.getLocalItem("patientMode")=="edit-patient"){
              this.router.navigate(['/patient-search'], { queryParams: { mode: 'edit-patient' } });
              }else{
                this.router.navigate(['/flowsheet-book-appointment'],{queryParams:{id:this.sharedService.getLocalItem("patientMode")}});
              }
             
          }, 
          err => {
            this.toastr.errorToastr("please contact system admin!", 'Error!');
            console.log(err);
          }
      );
  }
  getAllergies() {
    return this.allergyArray;
  }
  getImmunizations(){
    return this.immunizationArray;
  }
  getAlert(){
    var alert: Alert = {
      ID:'0',
      PatientID: this.patientId,
      Code: this.alert_FormGroup.value["alert_Code"],
      Description: this.alert_FormGroup.value["alert_Description"]
    };
    return alert;
  }

  getSocial(){
    return this.socailArray
  }
  getFamily(){
    return this.familyArray
    
  }
  addAllergies(){
    var alergy: Allergies = {
      ID:'0',
      PatientId: this.patientId,
      Code: this.allergies_FormGroup.value["allergies_Code"],
      Description: this.allergies_FormGroup.value["allergies_Description"]
    };
    this.allergyArray.push(alergy);
    this.allergies_FormGroup.reset();
  }
  deleteRow(index){
    this.allergyArray.splice(index,1);
  }

  addSocials(){
    var social: Socials = {
      ID:'0',
      PatientID: this.patientId,
      Addiection: this.social_FormGroup.value["social_Addiection"],
      Frequency: this.social_FormGroup.value["social_Frequency"],
      Duration: this.social_FormGroup.value["social_Duration"]
    };
    
    this.socailArray.push(social);
    this.social_FormGroup.reset();
  }
  deleteSocailRow(index){
    this.socailArray.splice(index,1);
  }
  addfamilys(){
    var family: Families = {
      ID:'0',
      PatientID: this.patientId,
      Member: this.family_FormGroup.value["family_Member"],
      Diseases: this.family_FormGroup.value["family_Diseases"]
    };
    this.familyArray.push(family);
    this.family_FormGroup.reset();
  }
  deleteFamilyRow(index){
    this.familyArray.splice(index,1);
  }
 
  
  getMasterData(key){
    this.patientCreateService.getMasterData(key)
    .subscribe
    (
        res => {this.alertKeywordArray = res; }, 
        err => {console.log(err);}
    );
  }
  onOpenCalendar(container) {
    container.monthSelectHandler = (event: any): void => {
      container._store.dispatch(container._actions.select(event.date));
    };     
    container.setViewMode('year');
   }
   addImmunizations(){
    var immunization: Immunizations = {
      ID:'0',
      PatientId: this.patientId,
      Date:this.immunizations_FormGroup.value["immunizations_Date"].substring(5,7)+"-"+this.immunizations_FormGroup.value["immunizations_Date"].substring(8,10)+
      "-"+this.immunizations_FormGroup.value["immunizations_Date"].substring(0,4) ,
      Code: this.immunizations_FormGroup.value["immunizations_Code"],
      Description: this.immunizations_FormGroup.value["immunizations_Description"]
    };
    this.immunizationArray.push(immunization);
    this.immunizations_FormGroup.reset();

  }
  deleteImmunizationRow(index){
    this.immunizationArray.splice(index,1);
  }
  cancel(){
    if(this.sharedService.getLocalItem("patientMode")=="edit-patient"){
    this.router.navigate(['/patient-search'], { queryParams: { mode: 'edit-patient' } });
    }else{
      this.router.navigate(['/flowsheet-book-appointment'],{queryParams:{id:this.sharedService.getLocalItem("patientMode")}});
    }
  }
}

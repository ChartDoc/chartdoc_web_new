
export class Alert {
    ID: string;
    PatientID : string;
    Code: string;
    Description: string;
}

export class Allergies
{
	ID: string;
	PatientId: string;
	Code: string;
    Description: string;
}

export class Immunizations
{
	ID: string;
	PatientId: string;
	Code: string;
	Description: string;
	Date: string;
}

export class Socials{
    ID: string;
    PatientID : string;
    Addiection: string;
	Frequency: string;
	Duration: string;
}
export class Families{
    ID: string;
    PatientID : string;
    Member: string;
	Diseases: string;
}

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedService } from 'src/app/core/shared.service';
import { TimeGrid } from '@fullcalendar/timegrid';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  isProvider: boolean = true;
  isMedicalStaff: boolean;
  isOfficeStaff: boolean;
  formData: FormData;
  files: File[] = [];
  tempArray: string[];
  public width: number = 400;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  @ViewChild('file', { static: true }) myInputVariable: ElementRef;
  @ViewChild('roleForm', { static: true }) roleForm;
  roleType = [];
  medicalRoleType: [];
  officeRoleType: [];
  speciality = [];
  role = [];
  RoleType: string;
  specialityType: string;
  imageFilePath: File = null;
  selectedId: number;
  editUserData: any;
  disableSaveButton: boolean = false;
  RoleArray: Array<string> = [];
  fileUrl: string = "";
  isEdit: boolean = false;
  createNewUserFormGroup = new FormGroup({
    fullName: new FormControl('', [Validators.required]),
    dateOfBirth: new FormControl('', [Validators.required]),
    ssn: new FormControl('', [Validators.required]),
    phone: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.pattern(this.emailPattern)])
  });
  roleFormGroup = new FormGroup({
    // userType:new FormControl(''),
    RoleType: new FormControl(''),
    specialtyType: new FormControl(''),
    Roles: new FormArray([]),

  })

  constructor(private userService: UserService, public toastr: ToastrManager, private _avRoute: ActivatedRoute, private sharedService: SharedService, private router: Router) {
    this.formData = new FormData();
    if (this._avRoute.snapshot.queryParams["id"] != undefined && this._avRoute.snapshot.queryParams["id"] == "1") {

      if (this.sharedService.getLocalItem("userDetails").Id != null) {
        this.isEdit = true;
        this.disableSaveButton = true;
        this.editUserData = this.sharedService.getLocalItem("userDetails");
        let userdob = new Date(this.editUserData.DateOfBirth);
        this.fileUrl = this.editUserData.DoctorImage;
        userdob.setMinutes(userdob.getMinutes() + userdob.getTimezoneOffset());
        userdob.setDate(userdob.getDate() + 1);
        this.createNewUserFormGroup.patchValue({
          fullName: this.editUserData.DoctorName,
           dateOfBirth: "1991-03-02",
          ssn: this.editUserData.SSN,
          phone: this.editUserData.Phone,
          email: this.editUserData.Email
        })
        this.setradio(this.editUserData.RoleType)
        this.createNewUserFormGroup.patchValue({
          dateOfBirth: userdob,//doctorBookingInfo.dateOfBirth,   
        })
      }
      this.roleFormGroup.patchValue({
        RoleType: this.editUserData.RoleSubType,
        specialtyType: this.editUserData.specialtyType,

      })
      var arr: string[] = this.editUserData.RoleList.split(",").map((item: string) => item.trim());
      console.log("after conversion=", arr)
    }
    else {
      this.setradio(1);
    }

  }

  isSelected(Id: string): boolean {
    if (this._avRoute.snapshot.queryParams["id"] != undefined) {
      var arr: string[] = this.editUserData.RoleList.split(",").map((item: string) => item.trim());
      const formArray: FormArray = this.roleFormGroup.get('Roles') as FormArray;
      var status: boolean;
      status = arr.indexOf(Id) >= 0 ? true : false;
      if (status) {

        formArray.push(new FormControl(Id));
      }
      return status
    }
    else {
      return false;
    }

  }

  ngOnInit() {
    // this.removeDuplicateElement()
  }
  removeDuplicateElement() {
    var arr: Array<string> = [];
    const formArray: FormArray = this.roleFormGroup.get('Roles') as FormArray;

    var i;
    for (i = 0; i < this.tempArray.length; i++) {
      var data = this.tempArray[i];
      if (i == 0) {
        arr.push(data);
      }
      else {

        if (arr.indexOf(data) == -1) {
          arr.push(data);
          // formArray.push(parseInt(data));
        }
      }
    }

    //  this.roleFormGroup.controls['Roles'].value.

  }
  // image: new FormControl('')
  Roles(event) {
    const formArray: FormArray = this.roleFormGroup.get('Roles') as FormArray;
    if (event.target.checked) {
      // Add a new control in the arrayForm
      formArray.push(new FormControl(event.target.value));
    }
    /* unselected */
    else {
      // find the unselected element
      let i: number = 0;

      formArray.controls.forEach((ctrl: FormControl) => {
        if (ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }

        i++;
      });
    }

    // alert("data="+JSON.stringify(formArray))
  }
  selectProviderChangeHandler(event) {
    this.specialityType = event.target.value;
  }

  validatePhoneno() {
    // /^(\()?\d{3}(\))?(-|\s)?-\d{3}-\d{4}$/;///^\d{3}-\d{3}-\d{4}$/;

    var phoneNumber = this.createNewUserFormGroup.controls["phone"].value;
    var phoneRGEX = /^(\()\d{3}(\))(-|\s)?-\d{3}-\d{4}$/;
    var phoneResult = phoneRGEX.test(phoneNumber);
    //alert("phone:" + phoneResult);
    return phoneResult
  }
  formatphone() {
    var phoneNumber = this.createNewUserFormGroup.controls["phone"].value;
    phoneNumber = phoneNumber.replace(/\D+/g, '').replace(/(\d{3})(\d{3})(\d{4})/, '($1)-$2-$3');
    this.createNewUserFormGroup.patchValue({
      phone: phoneNumber
    })
  }



  setradio(id: number) {

    if (id == 1) {
      this.getRoleType(id);
      this.getSpeciality();
      this.getRole();
      this.isProvider = true;
      this.isMedicalStaff = false;
      this.isOfficeStaff = false,
        this.selectedId = 1;

    }
    else if (id == 2) {
      this.getRoleType(id);
      this.getRole();
      this.isProvider = false;
      this.isMedicalStaff = true;
      this.isOfficeStaff = false;
      this.selectedId = 2;
    }
    else if (id == 3) {
      this.getRoleType(id);
      this.getRole();
      this.isProvider = false;
      this.isMedicalStaff = false;
      this.isOfficeStaff = true;
      this.selectedId = 3;
    }

  }
  getRoleType(id: number) {
    this.roleType = [];
    this.medicalRoleType = [];
    this.officeRoleType = [];
    this.userService.getRoleType(id).subscribe((res) => {
      if (id == 1) {
        this.roleType = res;
        console.log("provider Data=",this.roleType);
      }
      else if (id == 2) {
        this.medicalRoleType = res;
        console.log("data="+JSON.stringify(res));
      }
      else if (id == 3) {
        this.officeRoleType = res;
      }
    })
  }
  getSpeciality() {
    this.userService.getSpeciality().subscribe((res) => {
      this.speciality = res;

    })
  }
  getRole() {
    this.userService.getRole().subscribe((res) => {
      this.role = res;
      console.log("role=", this.role)
    })
  }

  selectChangeHandler(event: any) {
    //update the ui
    this.RoleType = event.target.value;
  }

  save() {

    this.addUserDetails();
    //alert("save button")
    console.log("form value" + this.createNewUserFormGroup);

  }
  addUserDetails() {
    if (this._avRoute.snapshot.queryParams["id"] != undefined && this._avRoute.snapshot.queryParams["id"] == "1") {
      this.editUserDetails()
    }
    else {
      this.addNewUserDetails()
    }
  }
  addNewUserDetails() {

    if (this.createNewUserFormGroup.valid) {
      let roleList = (this.roleFormGroup.controls['Roles'].value).toString();
      // image:this.createNewUserFormGroup.controls['image'].value,
      var requestInfo = {
        Id: "0",
        FULLName: this.createNewUserFormGroup.controls['fullName'].value,
        DateOfBirth: this.createNewUserFormGroup.controls['dateOfBirth'].value,
        SSN: this.createNewUserFormGroup.controls['ssn'].value,
        Phone: this.createNewUserFormGroup.controls['phone'].value,
        Email: this.createNewUserFormGroup.controls['email'].value,
        RoleType: this.selectedId.toString(),
        // specialityType:this.roleFormGroup.controls['specialtyType'].value,
        specialityType: this.specialityType,
        RoleList: roleList,
        RoleSubType: this.roleFormGroup.controls['RoleType'].value,
      }
      console.log("data=" + JSON.stringify(requestInfo))
      this.formData.append("userDetails", JSON.stringify(requestInfo));
      this.formData.append("uploadFile", this.files[0]);
      this.userService.addUser(this.formData).subscribe((res) => {
        console.log("response=" + JSON.stringify(res));
        this.router.navigate(['/manage-user']);
        this.toastr.successToastr("New user created successfully ", 'success!');
        this.createNewUserFormGroup.reset();
        this.roleForm.reset();

      }, err => {

        console.log(err);
      })
    }
    else {
      this.toastr.errorToastr("Please fill all the fields ", 'Oops!');
    }
    // console.log("request Info=" + JSON.stringify(requestInfo));

  }
  editUserDetails() {
    if (this.createNewUserFormGroup.valid) {
      let tempList = (this.roleFormGroup.controls['Roles'].value);
      let roleList:Array<number>=[];
      roleList=tempList.reduce((unique, item) => {
        console.log(
          item,
          unique,
          unique.includes(item),
          unique.includes(item) ? unique : [...unique, item],
        );
        
        return unique.includes(item) ? unique : [...unique, item]
      }, []).toString();
      
      // image:this.createNewUserFormGroup.controls['image'].value,
      var requestInfo = {
        Id: this.editUserData.Id,
        FULLName: this.createNewUserFormGroup.controls['fullName'].value,
        DateOfBirth: this.createNewUserFormGroup.controls['dateOfBirth'].value,
        SSN: this.createNewUserFormGroup.controls['ssn'].value,
        Phone: this.createNewUserFormGroup.controls['phone'].value,
        Email: this.createNewUserFormGroup.controls['email'].value,
        RoleType: this.selectedId.toString(),
        // specialityType:this.roleFormGroup.controls['specialtyType'].value,
        specialityType: this.specialityType,
        RoleList: roleList,
        RoleSubType: this.roleFormGroup.controls['RoleType'].value,
      }
      console.log("data=" + JSON.stringify(requestInfo))
      this.formData.append("userDetails", JSON.stringify(requestInfo));
      this.formData.append("uploadFile", this.files[0]);
      this.userService.addUser(this.formData).subscribe((res) => {
        console.log("response=" + JSON.stringify(res));
        this.router.navigate(['/manage-user']);
        this.toastr.successToastr("New user created successfully ", 'success!');
        this.createNewUserFormGroup.reset();
        this.roleForm.reset();

      }, err => {

        console.log(err);
      })
    }
    else {
      this.toastr.errorToastr("Please fill all the fields ", 'Oops!');
    }
  }
  cancel() {
    this.createNewUserFormGroup.reset();
    this.roleForm.reset();

  }
}

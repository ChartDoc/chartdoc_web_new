import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonService } from './common.service';
import { SharedService } from './shared.service';
import { httpInterceptorProviders } from './http-interceptor';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { DndDirective } from './file-upload/dnd.directive';
import { ProgressComponent } from './file-upload/progress/progress.component';

@NgModule({
  declarations: [FileUploadComponent, DndDirective, ProgressComponent],
  imports: [
    CommonModule
  ],
  providers: [
    CommonService,
    SharedService
  ],
  exports: [FileUploadComponent, DndDirective, ProgressComponent]
})
export class CoreModule { }
